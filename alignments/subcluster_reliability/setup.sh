#!/bin/bash

modPath=`cat ../../pathsAndExecutables.txt | grep "^modellerPath" | cut -f2 -d= | sed 's/ //g'`
modExe=`cat ../../pathsAndExecutables.txt | grep "^modellerExecutable" | cut -f2 -d= | sed 's/ //g'`
ii=0
for file in `cat ../../queryClusters/random$2/$1`; do	
	mkdir -p dir_$1
	${modPath}/${modExe} align3d_subclusters.py $1 ${ii} $2
	mv align3d_subclusters.log log.${file} 	
	rm *.ali
	mv log.${file} dir_$1
	ii=$(($ii+1))
done

