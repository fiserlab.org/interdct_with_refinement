#!/bin/bash

modPath=`cat ../../pathsAndExecutables.txt | grep "^modellerPath" | cut -f2 -d= | sed 's/ //g'`
modExe=`cat ../../pathsAndExecutables.txt | grep "^modellerExecutable" | cut -f2 -d= | sed 's/ //g'`

for file in `cat ../../pdbsAndQueries.txt`; do
	${modPath}/${modExe} align3d_singleTemplates.py ${file}
	mv align3d_singleTemplates.log log.${file} 	
	rm *.ali
done

