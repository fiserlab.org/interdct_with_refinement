# This will align 3D structures of two proteins:

import sys
from modeller import *
log.verbose()
env = environ()
env.io.atom_files_directory = ['../../structures',\
                               '../../FASTAbyChain']


Q = [] 
file = '../../pdbs.txt'
fh = open(file)
for f in fh:
    Q.append(f.strip())
fh.close()

for i in [sys.argv[1]]:
    for j in Q:
        if i != j:
            aln = alignment(env)
            mdl = model(env)

            mdl.read(file=i, model_segment=('FIRST:@', 'END:'))
            aln.append_model(mdl, align_codes=i, atom_files=i)

            mdl.read(file=j, model_segment=('FIRST:@', 'END:'))
            aln.append_model(mdl, align_codes=j, atom_files=j)

            aln.align(gap_penalties_1d=[-600, -400])
            aln.align3d(gap_penalties_3d=[0, 4.0])
            aln.write(file=str(j)+'-'+str(i)+'-str.ali')


