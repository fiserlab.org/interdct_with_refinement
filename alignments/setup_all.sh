#!/bin/bash

directories=( "random1"  "random10"  "random2"  "random3"  "random4"  "random5"  "random6"  "random7"  "random8"  "random9")
cd queries
bash setup.sh
for dir in ${directories[@]}; do
	cd ../${dir}
	bash setup.sh
done
