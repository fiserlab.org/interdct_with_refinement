import os

homeDir = os.getcwd()


os.system(f'rm {homeDir}/alignments/singleTemplates/log*')
os.system(f'rm -r {homeDir}/changedBfactorMapcount/singleTemplates/align*')
os.system(f'rm -r {homeDir}/predIntAlign/align*')
os.system(f'rm -r {homeDir}/predIntAlign/interfaceResMappedCounts')

