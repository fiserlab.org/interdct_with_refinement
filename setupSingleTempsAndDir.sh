#!/bin/bash

cd alignments/singleTemplates
bash setup.sh

cd ../../changedBfactorMapcount/singleTemplates
bash setup.sh

cd ../../predIntAlign
bash setup.sh
