import glob
import sys
import numpy as np
import os 

homeDir = os.getcwd()

queries = []
# file1 = f'{homeDir}/pdbs.txt'
file1 = f'{homeDir}/pdbsAndQueries.txt'
fh1 = open(file1)
for f1 in fh1:
	queries.append(f1.strip())
fh1.close()


for query in queries:
	templates = []
	file2 = f'{homeDir}/pdbs.txt'
	fh2 = open(file2)
	for f2 in fh2:
		templates.append(f2.strip())
	fh2.close()

	skip = ''
	for t in templates:
		if query == t:
			skip = t
	if skip != '':
		templates.remove(skip)

	sa_residues = []
	saPDB = open(f'{homeDir}/SA_PDBs/{query}.SA')
	for sPDB in saPDB:
		sPDB = sPDB.strip()
		if sPDB[22:26].strip() not in sa_residues:
			sa_residues.append(int(sPDB[22:26]))
	saPDB.close()


	log = []
	file = f'{homeDir}/alignments/singleTemplates/log.{query}'
	fh = open(file)
	i = 0
	indexStart = []
	indexEnd = []
	for f in fh: 
		f = f.strip()
		log.append(f)
		if f'#  N av ds st dv   {query[0:6]}' in f:
			indexStart.append(i)
		if f'{query}-str.ali' in f:
			indexEnd.append(i)
		i += 1
	fh.close()


	cc = 0
	while cc < len(templates):
		mappedResidueCount = []
		uniqueResidues = []
		interface = []
		filetem = glob.glob(f'{homeDir}/intercaatResults/{templates[cc]}*')[0]
		fhtem = open(filetem)
		for ftem in fhtem:
			interface.append(ftem.strip())
		fhtem.close()

		predIntAlin = np.zeros((len(interface),3))
		predIntAlin.fill(-1)
		ccc = 0
		while ccc < len(interface):
			predIntAlin[ccc][0] = int(interface[ccc])
			predIntAlin[ccc][2] = ccc
			ccc += 1

		c = indexStart[cc]
		eqRes = []
		while c < indexEnd[cc]:
			if log[c][0:1] != '#':
				hold = log[c].split()
				if '*' in hold:
					if hold[7] in interface:
						if hold[5] not in uniqueResidues:
							mappedResidueCount.append(1)
							uniqueResidues.append(hold[5])
							
							ccc = 0
							while ccc < len(predIntAlin):
								if predIntAlin[ccc][0] == int(hold[7]) and int(hold[5]) in sa_residues:
									predIntAlin[ccc][1] = int(hold[5])
								ccc += 1

				else:
					if hold[6] in interface:
						if hold[4] not in uniqueResidues:
							mappedResidueCount.append(1)
							uniqueResidues.append(hold[4])

							ccc = 0
							while ccc < len(predIntAlin):
								if predIntAlin[ccc][0] == int(hold[6]) and int(hold[4]) in sa_residues:
									predIntAlin[ccc][1] = int(hold[4])
								ccc += 1
			c += 1
		

		newFile = open(f"{homeDir}/predIntAlign/alignedTo.{templates[cc]}/{query}.ali", "x")
		ccc = 0
		while ccc < len(predIntAlin):
			newFile.write(f'{predIntAlin[ccc][0]}  {predIntAlin[ccc][1]}  {predIntAlin[ccc][2]}\n')
			ccc += 1
		newFile.close()

		newFile = open(f"{homeDir}/changedBfactorMapcount/singleTemplates/alignedTo.{templates[cc]}/{query}.SA.changedBfactorMapcount", "x")
		filePDB = f'{homeDir}/SA_PDBs/{query}.SA'
		fhPDB = open(filePDB)
		for fPDB in fhPDB:
			fPDB = fPDB.strip()
			if fPDB[22:26].strip() in uniqueResidues:
				newBFactor = str(mappedResidueCount[uniqueResidues.index(fPDB[22:26].strip())])
				newFile.write('{0}{1:>6}{2:>6}\n'.format(fPDB[0:54],'1.00',newBFactor+'.00'))
			else:
				newFile.write('{0}{1:>6}{2:>6}\n'.format(fPDB[0:54],'1.00','0.00'))
		newFile.close()

		cc += 1


for query in queries:
	files = glob.glob(f'{homeDir}/predIntAlign/alignedTo.{query}/*')
	count = []
	index = []
	cc = 0
	for file in files:
		fh = open(file)
		c = 0
		for f in fh:
			f = f.split()
			if int(float(f[1])) == -1:
				if cc == 0:
					count.append(0)
					index.append(int(float(f[2])))
			else:
				if cc == 0:
					count.append(1)
					index.append(int(float(f[2])))
				else:
					count[c] += 1
			c += 1
		cc += 1
		fh.close()
	newFile = open(f"{homeDir}/predIntAlign/interfaceResMappedCounts/{query}.ResCounts", "x")
	cc = 0
	while cc < len(index):
		newFile.write(f'{count[cc]}   {index[cc]}\n')
		cc +=1
	newFile.close()


# ### Check interface lengths
# files = glob.glob(f'{homeDir}/predIntAlign/interfaceResMappedCounts/*')
# length = []
# for fff in files:
# 	fh = open(fff)
# 	c = 0
# 	for f in fh:
# 		c += 1
# 	print(fff.split('/')[-1],c)
# 	fh.close()

