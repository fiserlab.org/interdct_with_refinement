import os

homeDir = os.getcwd()

pdb = []
qc = []
file = f'{homeDir}/queries.txt'
fh = open(file)
for f in fh:
    f = f.strip()
    pdb.append(f.split('.')[0])
    qc.append(f.split('.')[1])
fh.close()

d = {'CYS': 'C', 'ASP': 'D', 'SER': 'S', 'GLN': 'Q', 'LYS': 'K',
     'ILE': 'I', 'PRO': 'P', 'THR': 'T', 'PHE': 'F', 'ASN': 'N', 
     'GLY': 'G', 'HIS': 'H', 'LEU': 'L', 'ARG': 'R', 'TRP': 'W', 
     'ALA': 'A', 'VAL':'V', 'GLU': 'E', 'TYR': 'Y', 'MET': 'M'}
c = 0
while c < len(pdb):
    file = f"{homeDir}/structures/{pdb[c]}.{qc[c]}.pdb"
    newFile = open(f'{homeDir}/FASTAbyChain/{pdb[c]}.{qc[c]}.fasta','w')
    fh = open(file)
    hold = []
    newFile.write(f'>{pdb[c]}:{qc[c]}\n')
    seq = ''
    for f in fh:
    	f = f.strip()
    	if f[0:4] == 'ATOM':
            if int(f[22:26]) not in hold:
                seq += d[f[17:20]]
                hold.append(int(f[22:26]))
    fh.close()
    newFile.write(seq)
    newFile.close()
    os.system(f'cp {homeDir}/FASTAbyChain/{pdb[c]}.{qc[c]}.fasta {homeDir}/conservationAnalysis_local/blastResults')
    c += 1
