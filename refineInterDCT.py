import os

homeDir = os.getcwd()

queries = []
file = f'{homeDir}/queries.txt'
fh = open(file)
for f in fh:
	queries.append(f.strip())
fh.close()

# Runs zdock on all queries with all probes
for q in queries:
	q = q.split('.')
	os.system(f'bash zdockSetup.sh {q[0]} {q[1]}')



# Gets interfaces of all zdock complexes 
os.system('cat zdockInterfaceJobs.txt | parallel -j 60% -I \{\}')

# removes temporary file
os.system('rm zdockInterfaceJobs.txt')

# Analyzes zdock complexes
os.system('python3 checkInterface.py')

# Finds best zdock complex and gives final interface prediction 
os.system('python3 findComplex.py')

