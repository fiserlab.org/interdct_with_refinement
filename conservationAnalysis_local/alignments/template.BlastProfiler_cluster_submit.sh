#!/bin/sh

#$ -S /bin/bash
#$ -V 
#$ -cwd
#$ -j y
#$ -r y
#$ -N blast
#$ -t 1-1
#$ -o LOGFILES
  
### here you list your tasks like  (task1 task2 ... ) separated by blanks
tasks=(30-90-30-30-60-1a3rL)

## this is default, do not touch
jobindex=$((SGE_TASK_ID-1));
input=${tasks[$jobindex]}
wd=$PWD

# parse the input
sl=`echo $input | awk -F'-' '{print $1}'`
su=`echo $input | awk -F'-' '{print $2}'`
fl=`echo $input | awk -F'-' '{print $3}'`
ll=`echo $input | awk -F'-' '{print $4}'`
pu=`echo $input | awk -F'-' '{print $5}'`
pdb_id=`echo $input | awk -F'-' '{print $6}'`
pdb_id_orig="origid"

# copy required files to remote directory
tempdir=/tmp/steven/$input"_"$JOB_ID
rm -fr $tempdir; mkdir -p $tempdir
cd $tempdir

# code goes here

# 1. Generate BlastProfiler alignments

# Directories containing input and output for BlastProfiler
hub_dir="/home/steven/targetSelection/interfacePrediction_empty/conservationAnalysis_local"

fastadir="$hub_dir/blastResults"
psiblastdir="$fastadir" # ParseBlast needs to be in this directory
fastafile=$pdb_id
psiblastfile=$pdb_id-blastOut

alnhubdir="$hub_dir/alignments"
alnoutdir="$alnhubdir/$pdb_id-alns"
mkdir -p $alnoutdir

# Run BlastProfiler
if [ $su -gt $sl ]; then 
    cp $psiblastdir/$psiblastfile .

    lo_seqid_cutoff=$sl # default 30
    hi_seqid_cutoff=$su # default 90
    
    lo_length_cutoff=$ll # default 30
    lo_pct_query_length_cutoff=$fl # default 30
    hi_seqid_alnseq_cutoff=$pu # default 100

    eval_cutoff=1e-10 # default 1e-4
    
    of=$pdb_id.sl.$sl.su.$su.fl.$fl.ll.$ll.pu.$pu.out
		
    $psiblastdir/ParseBlastv12_fixedIter.pl -i $fastadir/$fastafile -o $psiblastfile -sl $lo_seqid_cutoff -su $hi_seqid_cutoff -ll $lo_length_cutoff -fl $lo_pct_query_length_cutoff -pu $hi_seqid_alnseq_cutoff -el $eval_cutoff -af fasta -of $of
    of_nseqs=`cat $of.aln | grep ">" | wc -l`

    echo "Generated alignment file" $of.aln "containing" $of_nseqs "sequences"
    mv $of.aln $alnoutdir
    rm -f $psiblastfile
fi


## now, remove the temp dir on the node
cd
rm -rf $tempdir


