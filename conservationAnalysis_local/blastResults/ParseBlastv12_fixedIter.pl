#!/usr/bin/perl

# compared to v10, it maintains the complete sequence id for each 
# hit, even when it starts with gi

use Getopt::Long;
use File::Basename;

my $cdHitPath  = `cat pathsAndExecutables.txt | grep "^cdhitPath" | cut -f2 -d= | sed 's/ //g'`; # path to clustering program
chomp($cdHitPath);
my $clustalPath = `cat pathsAndExecutables.txt | grep "^clustalwPath" | cut -f2 -d= | sed 's/ //g'`; # path to clustalw
chomp($clustalPath);
my $clustalExe = `cat pathsAndExecutables.txt | grep "^clustalwExecutable" | cut -f2 -d= | sed 's/ //g'`; #clustalw executable
chomp($clustalExe);
my $tempWriteDir = ".";    # directory to write temporary files
#my $tempWriteDir = "/var/www/html/tmp";    # directory to write temporary files

my $clustal = 0;
my $sequences = 0;
my $alnments = 0;

GetOptions (
           "-i=s"	=>	\$inpseq,
	   "-o=s"	=>	\$pbout,
	   "-sl=i"	=>	\$seqidlocut,
	   "-su=i"	=>	\$seqidupcut,
	   "-el=f"	=>	\$evaluelocut,
	   "-ll=i"	=>	\$lengthlocut,
	   "-fl=i"	=>	\$qfraclocut,
	   "-pu=i"	=>	\$pairseqidupcut,
	   "-sf=s"	=>	\$seqformat,
	   "-sc=i"	=>	\$seq_ncols,
	   "-af=s"	=>	\$alnformat,
	   "-ac=i"	=>	\$aln_ncols,
	   "clst"	=>	\$clustal,
	   "-cf=s"	=>	\$clustformat,
	   "seq"	=>	\$sequences,
	   "aln"	=>	\$alnments,
	   "-of=s"	=>	\$outfile,
           );

#-- Print usage
$usage = undef;
$usage .= "\n$0\n";
$usage .= "\t-i     = File containing input sequence\n";
$usage .= "\t-o     = File containing psi-blast output\n";
$usage .= "\t-sl    = Lower cut-off for sequence identity with query [default: 30]\n";
$usage .= "\t-su    = Upper cut-off for sequence identity with query [default: 90]\n";
$usage .= "\t-el    = Lower cut-off for E-value [default: 1e-4]\n";
$usage .= "\t-ll    = Lower cut-off for filtering alignments by length [default: 30]\n";
$usage .= "\t-fl    = Lower cut-off for filtering alignments by percentage of query length [default: 30]\n";
$usage .= "\t-pu    = Upper cut-off for sequence identity between aligned sequences [range: 40-100; default: 100]\n";
$usage .= "\t-seq   = Request sequence output\n";
$usage .= "\t-sf    = Format for sequence output [fasta|pir; default: fasta]\n";
$usage .= "\t-sc    = Number of columns for sequence output default: 70]\n";
$usage .= "\t-aln   = Request alignment output (default)\n";
$usage .= "\t-af    = Format for alignment output [clustal|pir|fasta; default: clustal]\n";
$usage .= "\t-ac    = Number of columns for alignment output default: 70]\n";
$usage .= "\t-clst  = Request ClustalW realignment for output\n";
$usage .= "\t-cf    = Format for ClustalW realignment output [clustal|pir|fasta; default: clustal]\n";
$usage .= "\t-of    = Output filename\n";
$usage .= "\n";

unless ( $inpseq && $pbout ){
   print $usage;
   exit 0;
}

#-- Check command line options
unless ( $seqidlocut ){ $seqidlocut = 30 }

unless ( $seqidupcut ){ $seqidupcut = 90 }

unless ( $evaluelocut ){ $evaluelocut = 0.0001 }

unless ( $lengthlocut ){ $lengthlocut = 30 }

unless ( $qfraclocut ){ $qfraclocut = 30 }

unless ( $pairseqidupcut ) { $pairseqidupcut = 100 }

unless ( $seqformat ) { $seqformat = "fasta" }

unless ( $alnformat ) { $alnformat = "clustal" }

unless ( $outfile ) { $outfile = fileparse($0, '.pl') . ".out" }

unless ( $seq_ncols ) { $seq_ncols = 70 }

unless ( $aln_ncols ) { $aln_ncols = 70 }

unless ( $clustal || $sequences || $alnments){  $alnments = 1; }

unless ( -e $pbout ){
   print "\nCould not find PSI-Blast output file: $pbout\n";
   exit 0;
}

unless ( -e $inpseq ){
   print "\nCould not find input sequence file: $inpseq\n";
   exit 0;
}


# -- identify number of iterations in psi-blast output file
  $iter = 0;
  open (PBFILE, "grep -n ^Searching $pbout |");
  while (<PBFILE>){
    chomp;
    $iter++;
  }
  close (PBFILE);

#  print "File $pbout contains output of $iter iterations\n";

#-- read original sequence
  $oseq = "";
  open (SEQ, "<$inpseq");
  while ($seq = <SEQ>){
    chomp $seq;
        if ( $seq =~ /^>gi\|/){
	   $oid = (split(/\|/, $seq))[1];
	   $oid =~ s/^/gi\:/;
	} elsif ( $seq =~ /^>/){
             $oid = (split(" ", $seq))[0];
	     $oid =~ s/^>//;
          } else {
               s/\s+//;
               $oseq .= $seq;
            }
  }
  close (SEQ);
  $olen = length $oseq;

# -- Start Parsing Psi-Blast output file
  
#  open OUTS1, ">parseBlastMessages";

  $iteration = 0; %qseqs = undef; %sseqs = undef; @hits = (); 
  open (INFILE, "<$pbout");
  while ($line = <INFILE>){

     # -- identify which iteration of psi-blast
     if ( $line =~ /^Result/ ) {
     chomp;
     $iteration++;
     }

     while ( $line =~ /^>/ ){
     push (@lines, $line);

     # --- continue reading thro' same filehandle until next sequence or end of iteration or end of file occurs
     while ($line = <INFILE>){
       last if ( $line =~ /^>/ || $line =~ /^Result/ || $line =~ /Database/ || eof INFILE );
       push (@lines, $line);
     }


    # --- once you are here you have all alignments of one sequence in array @lines
    # --- all manipulations done here with ONLY @lines
    $index = 0;
    while ($index <= $#lines){

      # -- Parse sequence identifier
      if ( $lines[$index] =~ /^>/ ){
           ($id) = split (/\s+/, $lines[$index]);
	   $id =~ s/^>//;
	   $id =~ s/\|/:/g; # pipe symbol is used later on to calculate gaps for insertion; id cannot have pipe characters
        }

      # -- Parse each segment of alignment for Scores
      if ($lines[$index] =~ /\s+Score = /){
        ($score, $evalue) = (split(" ",$lines[$index]))[2,7];
	$evalue =~ s/\,$//;

         for ($i = $index + 1; $i <= $#lines; $i++){

	    # -- Parse for identities
	    if ($lines[$i] =~ /\s+Identities/){
	      chomp $lines[$i];
	      ($identity, $positives) = (split(" ", $lines[$i]))[3,7];
	      $identity =~ s/[(%),]//g;
	      $positives =~ s/[(%),]//g;

	      $querystart = 0;
	      $sbjctstart = 0;
	      undef $qseq;
	      undef $sseq;
	    }

	   # -- Parse query sequence from alignment
	   if ($lines[$i] =~ /^Query/){
	      # -- Store starting number only if its first occurence
	      if ($querystart == 0) {
	         $qstartnum = (split(" ", $lines[$i]))[1]; 
	  	 $querystart = 1;
    	      }

	      ($qstarttmp,$qseqtmp, $qendnum) = (split(" ", $lines[$i]))[1,2,3];
	      $qseq = $qseq . $qseqtmp;
	   }

	   # -- Parse hit sequence from alignment
	   if ($lines[$i] =~ /^Sbjct/){
	      # -- Store starting number only if its the first occurence
	      if ($sbjctstart == 0) {
	         $sstartnum = (split(" ", $lines[$i]))[1]; 
		 $sbjctstart = 1;
 	      }

	      ($sstarttmp, $sseqtmp, $sendnum) = (split(" ", $lines[$i]))[1,2,3];
	      $sseq = $sseq . $sseqtmp;
	   }

	   # -- if there is another alignment, reset the counting index to continue for next 
	   #    segment of the same alignment
           if ($lines[$i] =~ /\s+Score =/){
	     $index = $i - 1;
	     last;
	   } elsif ( $i == $#lines ){
	       $index = $i;
	     }
         }

         $qlen = ( $qseq =~ tr/[A-Z]// );

         # -- Print results of one alignment before looping back to next alignment or sequence

         if ( $identity >= $seqidlocut && $identity <= $seqidupcut && 
	      $evalue <= $evaluelocut  &&
	      $qlen >= $lengthlocut &&
	      100*$qlen/$olen >= $qfraclocut ){

	    #-- Form hit details
	    $id1 = $id . "_$sstartnum" . "_$sendnum"; # modified id keeps start and ending points of alignment (thus, becomes unique)
            $hit = sprintf ">%-15s|%3d|%5d|%7.4g|%5d|%5d|%5d|%5d|%5d|%5d",$id1,$iteration,$score,$evalue,$identity,$positives,$qstartnum,$qendnum,$sstartnum,$sendnum;

	      
	      my $ref; # reference for array holding hits for a particular subject id
	      if($ref = $hitsFor{$id}){ # check for overlapping hits of same subject
#	        print OUTS1 "$id has several hits:\n  $hit\n";
		for(my $j = 0; $j < @$ref; $j++){
		  my $entry = $ref->[$j];
#		  print OUTS1 "  $entry\n";
		  if( overlap($entry, $sstartnum, $sendnum) ){
#		    print OUTS1 "    This entry is redundant (overlaps)\n";
		    deleteFromHits($entry); # delete entry in @hits
		    $qseqs{$entry} = ""; # delete all info for $entry (i.e., the older overlapping hit)
		    $sseqs{$entry} = "";
		    splice(@$ref, $j, 1);
		    $j--;
		  }
		} # end for
#		print OUTS1 "---------------------------------------------------\n\n";
		push(@$ref, $hit); # enter current hit
	      } # end if there are hits for current id
	      else{
	        $ref = [$hit];
	      }
	      
	      $hitsFor{$id} = $ref;
	      push(@hits, $hit);
	      $qseqs{$hit} = $qseq;
	      $sseqs{$hit} = $sseq;

         } # end if hit is within cut-off values

      } # end if line is " Score" line

    $index++;
    } # end while index is smaller than elements in @lines

     # -- end-of-sequence start with next
     $iteration++ if ( $line =~ /^Result/ );
     undef @lines;
     } # end while line starts with ">"
  } # end while there are lines to be read in input file
  close (INFILE);

  #-- adjust leading position with '+' for distinguishing gaps
  for ( $i = 0; $i <= $#hits; $i++ ){
     $hitbeg = (split(/\|/, $hits[$i]))[6] - 1;

     $qseqs{$hits[$i]} =~ s/^/chr(43) x $hitbeg/e;
     $sseqs{$hits[$i]} =~ s/^/chr(43) x $hitbeg/e;
  }


  #-- introduce gaps as required
  for ( $i = 0; $i < length($oseq); $i++ ){

     for ( $j = 0; $j <= $#hits; $j++ ){

        if ( substr($qseqs{$hits[$j]}, $i, 1) eq "-" ){

	   #-- introduce gap in original sequence
	   substr($oseq, $i, 0) = "-";

	   #-- adjust all other sequence for this position
	   for ($k = 0; $k <= $#hits; $k++ ){

	      if ( substr($qseqs{$hits[$k]}, $i, 1) ne "-" && $i < length($qseqs{$hits[$k]}) ){
	         substr($qseqs{$hits[$k]}, $i, 0 ) = "-";
	         substr($sseqs{$hits[$k]}, $i, 0 ) = "-";
	      }
	   }

	   last;
	}
     }

  }

  #-- replace '+'s with '-' and also adjust the total length
  for ( $i = 0; $i <= $#hits; $i++ ){
     $lendiff = length($oseq) - length($qseqs{$hits[$i]});

     $qseqs{$hits[$i]} =~ s/\+/\-/g;
     $sseqs{$hits[$i]} =~ s/\+/\-/g;

     $qseqs{$hits[$i]} =~ s/$/chr(45) x $lendiff/e;
     $sseqs{$hits[$i]} =~ s/$/chr(45) x $lendiff/e;
  }

  #-- filter database sequences in terms of seqid b/w them
  #my $tempOutfile = "$tempWriteDir/$pbout.seqs" . time . rand();
  my $tempOutfile = "$pbout.seqs" . time . rand();
  my $clusterFile = $tempOutfile . "cl";
  
  open OUTS, ">$tempOutfile" or die "cannot open $tempOutfile for writting\n";
  
  for ( $i = 0; $i <= $#hits; $i++ ){
     my $seq = $sseqs{$hits[$i]};
     $seq =~ s/-//g;
     print OUTS "$hits[$i]\n$seq\n";
  }

  close OUTS;
  ##### check if there are enough sequences before running cd-hit ####

  my $numSeqs = `grep -c "^>" $tempOutfile`;
  chomp($numSeqs);
#  $numSeqs = 0;
  
  if($numSeqs > 1){

    my $n = getN($pairseqidupcut); # get the "n" parameter for cd-hit program
  
    if($n){ # an invalid n value means that the value provided for $pairseqidupcut is out of range
  
      $pairseqidupcut = $pairseqidupcut/100; # cd-hit program takes fractional value, not percentage
      #if($pairseqidupcut >= 0.55 ){ 
        system "$cdHitPath/cd-hit -i $tempOutfile -o $clusterFile -n $n -c $pairseqidupcut -d 50 > $tempOutfile.dump";
      #}
      #else{
      #  system "$cdHitPath/mcd-hit -i $tempOutfile -o $clusterFile -n $n -c $pairseqidupcut -d 50 > $tempOutfile.dump";
      #}
    }
    else{
      die "ERROR: value for upper cut-off for sequence identity between aligned sequences should be in the 40-100 range\n$pairseqidupcut was entered\n";
    }
  } # end if numSeqs > 1
  else{ system "cp $tempOutfile $clusterFile"; }
 
  #-- keep the id lines for the hits selected with after clustering
  my @ids = `grep "^>" $clusterFile`;
  foreach my $id(@ids){
    chomp($id);
    $selectedIds{$id} = 1;
    
  }

  #-- remove unselected hits from the hits list (@hits)
  for(my $i = 0; $i < @hits; $i++){
    if(!$selectedIds{$hits[$i]}){
      splice(@hits, $i, 1);
      $i--;
    }
  }  
      
  
  if(-e $tempOutfile){ 
    system "rm $tempOutfile*";
  }  


  #-- reformat sequences to remove blanks caused by the previous  filter
  for ( $i = 0; $i < length($oseq); $i++ ){
     if ( substr($oseq, $i, 1) eq "-" ){
	$flag = 0;
        for ($j = 0; $j <= $#hits; $j++ ){
	   $flag = 1 if ( substr($sseqs{$hits[$j]}, $i, 1) ne "-" );
	}

	if ( $flag == 0 ){
	   substr($oseq, $i, 1) = "";
           for ($j = 0; $j <= $#hits; $j++ ){
	     substr($sseqs{$hits[$j]}, $i, 1) = "";
	   }
	   $i--;
	}
     }
  }
  # --- write alignment or sequences in appropriate format
 

    
    if($sequences){
      open(OUT, ">$outfile.seqs");
      printOutput("seq", $seq_ncols);
      close OUT;
    }
    if($alnments){
      open(OUT, ">$outfile.aln");
      printOutput("aln", $aln_ncols);
      close OUT;
    }
 
    realign() if ($clustal);






#-- Subroutines
# --- WriteSeqCols
# --- Input: input sequence as a single string, number of residues per line
# --- Output: returns a string with newlines inserted at sepcified columns
 
sub printOutput{
  my $otype = $_[0];
  $ncols = $_[1];
  my $oformat;
  if($otype eq "seq"){ $oformat = $seqformat; }
  else{                $oformat = $alnformat; }

  my $printseq = ""; # formated sequence; ready to print

    if ( $oformat =~ /fasta/i ){
       print OUT ">$oid\n";
       $printseq = WriteSeqCols("$oseq", $ncols, $otype);
       print OUT $printseq;

       foreach $hit ( @hits ){
          ## change semicolons in id to pipe signs, which is more standard
	  ($id, $rest) = split(/\|/, $hit, 2);
          $id =~ s/:/\|/g;
          print OUT "$id$rest\n";

#          print OUT "$hit\n";
	  $printseq = WriteSeqCols("$sseqs{$hit}", $ncols, $otype);
	  print OUT $printseq;
       }
    } # end if format is fasta
    elsif ( $oformat =~ /pir/i ){
         print OUT ">P1;$oid\n";
         print OUT "sequence:$oid::::::::\n";
         $printseq = WriteSeqCols("$oseq", $ncols, $otype);
         print OUT $printseq;
         print OUT "*\n";

         foreach $hit ( @hits ){
            $id = (split(/\|/, $hit))[0]; $id =~ s/^>//; $id =~ s/^gi://;
	    print OUT ">P1;$id\n";
	    print OUT "sequence:$id:",(split(/\|/,$hit))[8], "::",(split(/\|/, $hit))[9],":::::\n";
	    $printseq = WriteSeqCols("$sseqs{$hit}", $ncols, $otype);
	    print OUT $printseq;
            print OUT "*\n";
         } # end foreach
    } # end elsif (format is pir)
    else { # write output in clustalw format
      
        print OUT "CLUSTAL W format\n\n\n";
      
        $nlines = int(length($oseq)/$ncols);
        if(length($oseq)/$ncols != $nlines){
          $nlines++;
        }

        formatIds(); # get ids ready for clustalw format 
      
        for(my $i = 0; $i < $nlines; $i++){
          $currSeq = substr($oseq, $i * $ncols, $ncols);
	  print OUT "$oid$currSeq\n";
	  foreach my $hit(@hits){
	    $currId = $idOf{$hit};
	    $currSeq = substr($sseqs{$hit}, $i * $ncols, $ncols);
	    print OUT "$currId $currSeq\n";
	  } # end foreach
	  print OUT "\n\n";
        } # end for
    } # end else (format is clustalw)
      
} # end printOutput

sub realign{

      ### write temporary sequence file to be used as input for clustalw
      my $tempSeqFile = "$tempWriteDir/$outfile" . "_tempSeqs" . time;
      open(OUT, ">$tempSeqFile");
      my $otype = "seq"; # to print sequences to temporary sequence file
      my $ncols = 70;

      print OUT ">$oid\n";
      $oseq = WriteSeqCols("$oseq", $ncols, $otype);
      print OUT $oseq;

      foreach $hit ( @hits ){
         print OUT "$hit\n";
	 $sseqs{$hit} = WriteSeqCols("$sseqs{$hit}", $ncols, $otype);
	 print OUT $sseqs{$hit};
      } # end foreach
      close OUT;

      ### set command line for clustalW program #######
      my $clustalopts = "-infile=$tempSeqFile -outfile=$outfile.clustal "; # options for clustalw
      $clustformat =~ tr/A-Z/a-z/; # convert to all lowercase
      if($clustalformat !~ /clustal/i){
        $clustalopts .= "-output=$clustformat";
      }
      

      system "$clustalPath/$clustalExe $clustalopts > $tempSeqFile.out";
      #my $guideTree = $outfile; # name of guide tree created by clustalw (to be deleted) 
      system "rm $tempSeqFile*"

} # end realign


sub WriteSeqCols {

  # -- check arguments
  my $narg = 2;

  # -- assign arguments
  my ($seq, $cols, $otype) = @_;
  my ($nline, $n, $outseq);
  if($otype =~ /^seq*/i){
    $seq =~ s/-//g;
  }  

  undef $outseq; my $lastnewline = 0;
  $lastnewline = 1 if ( length($seq)/$cols == int(length($seq)/$cols) );

  $nline = int(length($seq)/$cols);
  for $n ( 1 .. $nline ) {
   $outseq .= substr($seq,($n-1)*$cols,$cols) . "\n";
  }

  $outseq .= substr($seq,$nline*$cols) . "\n" if ( $lastnewline == 0 );

return $outseq;
}

sub getN{
  my $result = 5;
  if($_[0] > 70 && $_[0] <= 100){ $result = 5; }
  elsif($_[0] <= 70 && $_[0] >= 55){ $result = 4; }
  elsif($_[0] < 55 && $_[0] >= 50){ $result = 3; }
  elsif($_[0] < 50 && $_[0] >= 40){ $result = 2; }
  else{ $result = 0; }

  return $result;
}

sub overlap{
  my $result = 0;
  my $maxAllowedOverlap = 0.5;
  my ($oldStart, $oldEnd) = (split(/\|/, $_[0]))[8, 9];
  my ($newStart, $newEnd) = ($_[1], $_[2]);
  
  if(! ( $newEnd < $oldStart) || ( $newStart > $oldEnd) ){
    my $startOverlap = $oldStart;
    if($newStart > $oldStart){ $startOverlap = $newStart; }

    my $endOverlap = $oldEnd;
    if($newEnd < $oldEnd){     $endOverlap = $newEnd; }

    my $lengthOld = $oldEnd - $oldStart;
    my $lengthOverlap = $endOverlap - $startOverlap;

    if( ($lengthOverlap/$lengthOld) > $maxAllowedOverlap){
      $result = 1;
    }
  }

  return $result;
}

sub deleteFromHits{
  for(my $i = 0; $i < @hits; $i++){
    if($hits[$i] eq $_[0]){
      splice(@hits, $i, 1);
      last;
    }
  }
}


sub formatIds{
  my $currLength = length($oid); # start by assuming that query has the longest id
  my $id = "";

  foreach my $hit(@hits){
    $id = (split(/\|/, $hit))[0];
    $id =~ s/:/\|/g; # substitute colons with pipe symbols (more standard)
    if(length($id) > $currLength){
      $currLength = length($id);
    }
  }

  my $lengthDiff = $currLength - length($oid) + 3;
  $oid =~ s/^>//;
  $oid =~ s/$/chr(32) x $lengthDiff/e;
  
  foreach my $hit(@hits){
    $id = (split(/\|/, $hit))[0];
    $id =~ s/:/\|/g; # substitute colons with pipe symbols (more standard)
    $lengthDiff = $currLength - length($id) + 3;
    $id =~ s/^>//;
    $id =~ s/$/chr(32) x $lengthDiff/e;
    $idOf{$hit} = $id;
  }
}
