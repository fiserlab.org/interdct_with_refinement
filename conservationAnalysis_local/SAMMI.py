import subprocess
import os
import sys

homeDir = f'{os.getcwd()}/conservationAnalysis_local'
inputfile = sys.argv[1]
leftBracket_char = '{'
rightBracket_char = '}'
quote_char = "'"
inputfile_alt = inputfile.split('.')[0]+':'+inputfile.split('.')[1]



### psiblast
# nr90 database
# blast
# python3
#####################################################################

psiblastPath = subprocess.check_output(["cat pathsAndExecutables.txt | grep \"^psiblastPath\" | cut -f2 -d= | sed 's/ //g'"],shell=True,text=True).strip()
nr90 = subprocess.check_output(["cat pathsAndExecutables.txt | grep \"^nr90\" | cut -f2 -d= | sed 's/ //g'"],shell=True,text=True).strip()

os.system(f'{psiblastPath}/psiblast -query {homeDir}/blastResults/{inputfile}.fasta -db {nr90} -evalue 1e-5 -num_iterations 3 -num_descriptions 20000 -num_alignments 20000 -out {inputfile}.fasta-blastOut')
os.system(f'mv {inputfile}.fasta-blastOut conservationAnalysis_local/blastResults')

#####################################################################


### Alignments 
# cd-hit
# clustalw
# perl
# python3 
# ParseBlastv12_fixedIter.pl
######################################################################
jobs = []
for sl in [60,55,50,45,40,35,30,25,20]:
	for su in [99,90,70,50]:
		for fl in [70]:
			for ll in [30]:
				for pu in [99,95,90,80,70,60,50,40]:
					if su > sl:
						jobs.append(f'{sl}-{su}-{fl}-{ll}-{pu}-{inputfile}.fasta')


os.system(f'mkdir -p {homeDir}/alignments/{inputfile}.fasta-alns')

for job in jobs:
	job = job.strip()
	job = job.split('-')
	of = f'{job[5]}.sl{job[0]}.su{job[1]}.fl{job[2]}.ll{job[3]}.pu{job[4]}.out'

	aln = subprocess.check_output([f'{homeDir}/blastResults/ParseBlastv12_fixedIter.pl', '-i', f'{homeDir}/blastResults/{inputfile}.fasta', '-o', \
		f'{homeDir}/blastResults/{inputfile}.fasta-blastOut', '-sl', job[0], '-su', job[1], '-fl', job[2], '-ll', job[3],  '-pu', job[4], '-el', \
		'0.0000000001',	'-af', 'fasta', '-of', of], shell =False, text= True)

	os.system(f'mv {of}.aln {homeDir}/alignments/{inputfile}.fasta-alns')
######################################################################


### SAMMI
# python3
# perl
# c++
# /MI_Scripts/aln_fasta_seqs_only_no_gaps_query.pl
# /MI_Scripts/aln_fasta_seqs_only_no_gaps_query_colshuffle_v2.pl
# /MI_Scripts/aln_mi_calc.cpp
# /MI_Scripts/aln_mi_colpair_diff.pl
######################################################################
jobs = []
for sl in [60,55,50,45,40,35,30,25,20]:
	for su in [99,90,70,50]:
		for fl in [70]:
			for ll in [30]:
				for pu in [99,95,90,80,70,60,50,40]:
					if su > sl:
						jobs.append(f'{sl}-{su}-{fl}-{ll}-{pu}-{inputfile}.fasta')



os.system(f'mkdir -p {homeDir}/mutualInformation/{inputfile}.fasta-mi-effaln50')
os.system(f'mkdir -p {homeDir}/mutualInformation/{inputfile}.fasta-mi-effaln50/{inputfile}.fasta-alns_seqsonly')
os.system(f'mkdir -p {homeDir}/mutualInformation/{inputfile}.fasta-mi-effaln50/{inputfile}.fasta-alns_seqsonly_colshuff')
os.system(f'mkdir -p {homeDir}/mutualInformation/{inputfile}.fasta-mi-effaln50/{inputfile}.fasta-mi_original')
os.system(f'mkdir -p {homeDir}/mutualInformation/{inputfile}.fasta-mi-effaln50/{inputfile}.fasta-mi_colshuff')
os.system(f'mkdir -p {homeDir}/mutualInformation/{inputfile}.fasta-mi-effaln50/{inputfile}.fasta-mi_diff')

c = 0
for job in jobs:
	job = job.strip()
	job = job.split('-')
	
	aln = f'{job[5]}.sl{job[0]}.su{job[1]}.fl{job[2]}.ll{job[3]}.pu{job[4]}.out'
	alnfile = f'{homeDir}/alignments/{inputfile}.fasta-alns/{aln}.aln'
	

	seqsonly_file = f'{homeDir}/mutualInformation/{inputfile}.fasta-mi-effaln50/{inputfile}.fasta-alns_seqsonly/{aln}.aln.seqsonly.original.tmp'
	seqsonly_file_colshuffle = f'{homeDir}/mutualInformation/{inputfile}.fasta-mi-effaln50/{inputfile}.fasta-alns_seqsonly_colshuff/{aln}.aln.seqsonly.colshuff.tmp'
	os.system(f'{homeDir}/MI_Scripts/aln_fasta_seqs_only_no_gaps_query.pl {alnfile} {inputfile_alt} > {seqsonly_file}')
	os.system(f'{homeDir}/MI_Scripts/aln_fasta_seqs_only_no_gaps_query_colshuffle_v2.pl {alnfile} {inputfile_alt} > {seqsonly_file_colshuffle}')

	seqcount = int(subprocess.check_output([f'cat {seqsonly_file} | wc -l'],shell=True, text= True))
	seqlength = int(subprocess.check_output([f'cat {seqsonly_file} | head -1 | awk {quote_char}{leftBracket_char}print length($0){rightBracket_char}{quote_char}'],shell=True, text= True))

	mi_aln_outfile = f'{homeDir}/mutualInformation/{inputfile}.fasta-mi-effaln50/{inputfile}.fasta-mi_original/{aln}.original.mi'
	mi_aln_colshuffle_outfile = f'{homeDir}/mutualInformation/{inputfile}.fasta-mi-effaln50/{inputfile}.fasta-mi_colshuff/{aln}.colshuff.mi'
	os.system(f'g++ -O3 -funroll-loops -fstrict-aliasing -o {homeDir}/aln_mi_calc_exe-{inputfile}-{c} {homeDir}/MI_Scripts/aln_mi_calc.cpp')
	os.system(f'{homeDir}/aln_mi_calc_exe-{inputfile}-{c} {seqsonly_file} {seqcount} {seqlength} > {mi_aln_outfile}')
	os.system(f'{homeDir}/aln_mi_calc_exe-{inputfile}-{c} {seqsonly_file_colshuffle} {seqcount} {seqlength} > {mi_aln_colshuffle_outfile}')
	os.system(f'rm {homeDir}/aln_mi_calc_exe-{inputfile}-{c}')

	mi_diff_outfile = f'{homeDir}/mutualInformation/{inputfile}.fasta-mi-effaln50/{inputfile}.fasta-mi_diff/{aln}.midiff'
	os.system(f'{homeDir}/MI_Scripts/aln_mi_colpair_diff.pl {mi_aln_outfile} {mi_aln_colshuffle_outfile} > {mi_diff_outfile}')

	ndata = int(subprocess.check_output([f'cat {mi_diff_outfile} | wc -l'],shell=True,text=True))
	ntopXpct = int(ndata*0.05)
	if ntopXpct > 0:
		avg_topXpct_mi = float(subprocess.check_output([f'cat {mi_diff_outfile} | awk {quote_char}{leftBracket_char}print $5{rightBracket_char}{quote_char} | sort -g | tail -{ntopXpct} | awk {quote_char}{leftBracket_char}sum+=$1{rightBracket_char}END{leftBracket_char}if(NR > 0) print sum/NR{rightBracket_char}{quote_char}'],shell=True, text= True))
		os.system(f'echo {aln}.aln {avg_topXpct_mi} >> {homeDir}/mutualInformation/{inputfile}.fasta-mi-effaln50/{inputfile}.fasta.all.aln.miscore')
	else:
		os.system(f'echo {aln}.aln >> {homeDir}/mutualInformation/{inputfile}.fasta-mi-effaln50/{inputfile}.fasta.all.aln.miscore')
	c += 1


######################################################################


### Residue Conservation
# python3
# python2
# score_conservation.py
# matrix directory in {homeDir}
######################################################################

python2Path = subprocess.check_output(["cat pathsAndExecutables.txt | grep \"^python2Path\" | cut -f2 -d= | sed 's/ //g'"],shell=True,text=True).strip()
python2Exe = subprocess.check_output(["cat pathsAndExecutables.txt | grep \"^python2Exe\" | cut -f2 -d= | sed 's/ //g'"],shell=True,text=True).strip()

aln = subprocess.check_output([f'cat {homeDir}/mutualInformation/{inputfile}.fasta-mi-effaln50/{inputfile}.fasta.all.aln.miscore | sort -gr -k2 | head -1 | awk {quote_char}{leftBracket_char}print $1{rightBracket_char}{quote_char}'],shell=True, text= True).strip()
os.system(f'{python2Path}/{python2Exe} {homeDir}/Conservation_JSD/score_conservation.py -g 0.7 -a {inputfile_alt} {homeDir}/alignments/{inputfile}.fasta-alns/{aln} > {homeDir}/scores/{inputfile_alt}.conservation')

######################################################################
