#!/usr/bin/perl

# takes difference between mutual information values of identical column pairs
# intended for use to compare MI between an alignment and its column-shuffled version

@ARGV == 2 or die("Usage: [aln_mi_mat] [aln_colshuffle_mi_mat]\n");

my $aln_mi_mat = $ARGV[0];
my $aln_mi_mat_colshuffle = $ARGV[1];

my @colpairs;
my @mi_mat;
my @mi_mat_colshuffle;

open(ALN_MI_MAT, $aln_mi_mat) or die("Can't open $aln_mi_mat\n");
while(my $line = <ALN_MI_MAT>){
    my @buf = split(/\s+/, $line);
    push @colpairs, $buf[1];
    push @mi_mat, $buf[4];

}
close ALN_MI_MAT;


open(ALN_MI_MAT_COLSHUFFLE, $aln_mi_mat_colshuffle) or die("Can't open $aln_mi_mat_colshuffle\n");
while(my $line = <ALN_MI_MAT_COLSHUFFLE>){
    my @buf = split(/\s+/, $line);
    push @mi_mat_colshuffle, $buf[4];

}
close ALN_MI_MAT_COLSHUFFLE;

#my @mi_diff;

for(my $i = 0; $i < scalar(@mi_mat); $i++){
    
    my $diff = $mi_mat[$i] - $mi_mat_colshuffle[$i];
    if ($diff < 0){$diff = 0;}
#    push @mi_diff, $diff;
    print "ColPair $colpairs[$i] MI_Diff = $diff bits\n";

}

