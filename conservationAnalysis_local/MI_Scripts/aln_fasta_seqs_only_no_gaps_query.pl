#!/usr/bin/perl

# extracts sequences from fasta file and prints them as block of text with one sequence per line
# subset_cols: return only subset of columns for use in MI calculations

use strict;
use warnings;
use Data::Dumper;
use Tie::IxHash;

@ARGV == 2 or die("Usage: [alignment] [query_pdb_id]");

my $alignment = $ARGV[0];
my $query_pdb_id = $ARGV[1];
#my $pdbfile = $ARGV[2];
#my $str_map_file = $ARGV[3];

my @seqids;
my @sequence_fragments;
my @sequences;

# READ IN FASTA ALIGNMENT

open(ALIGNMENT, $alignment) or die("Can't open $alignment\n");
while(my $line = <ALIGNMENT>) {

    chomp $line; 

    if ($line =~ /^>/) {
	$line =~ s/^>//;
	my @buf = split(/\s+/, $line);
	push @seqids, $buf[0];

	my $sequence_joined = join("", @sequence_fragments);
	push @sequences, $sequence_joined;
	@sequence_fragments = ();	

    }

    else {
	push @sequence_fragments, $line;
   }
 
}

# needed for last sequence
my $sequence_joined = join("", @sequence_fragments);
push @sequences, $sequence_joined;
@sequence_fragments = ();	

close ALIGNMENT;

shift(@sequences); # first element is blank, so remove it
#foreach (@sequences) {print $_,"\n";}

tie my %fasta_sequences, 'Tie::IxHash';
@fasta_sequences{@seqids} = @sequences;

#foreach (values %fasta_sequences) {print $_,"\n";}

my $aln_length = length($fasta_sequences{$query_pdb_id});
#print "Alignment length: $aln_length\n";
my $query_seq_wgaps = $fasta_sequences{$query_pdb_id};
my @cols_to_print;
for (my $i = 0; $i < length($query_seq_wgaps); $i++){
    unless(substr($query_seq_wgaps, $i, 1) eq "-") {push @cols_to_print, $i;} 
}
#print @cols_to_print;

foreach my $seq (@sequences){
    my $seq_length = length($seq);
    if ($seq_length == $aln_length){
	foreach my $col (@cols_to_print) {
	    print substr($seq, $col, 1); 
	}
	print "\n";
    }
}

