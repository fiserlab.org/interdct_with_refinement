#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <cstdlib>
#include <fstream> // ifstream
#include <cstring> // string
#include <stdlib.h> // exit

using namespace std;

int main (int argc, char **argv) {

  char* filename = argv[1];
  int seqcount = atoi(argv[2]);
  int seqlength = atoi(argv[3]);
  ifstream fin(filename);

  if (!fin.is_open())
    {
      cout << "Could not open file " << filename << endl;
      exit(0);
    }

     // READ IN SEQUENCES TO ARRAY

     char *seqarray[seqcount];          
     string buffer;

     getline(fin,buffer);

     int m = 0;
     while (!fin.eof())
       {
	 seqarray[m] = new char[seqlength];
	 strncpy( seqarray[m], buffer.c_str(), seqlength);
	 m++;
	 getline(fin,buffer);
       }  
     fin.close();
          
     // MUTUAL INFORMATION CALCULATION
     char aalist[20];
     int aa_alpha_size = 20;
     strcpy(aalist,"ACDEFGHIKLMNPQRSTVWY");
       
     for (int col_i = 0; col_i < seqlength; col_i++) {
       for (int col_j = col_i + 1; col_j < seqlength; col_j++) {
	 int bFlag = 0;
	       
	 int n_seqs = 0;
	 for(int seqindex = 0; seqindex < seqcount; seqindex++) {

	   char aa1 = seqarray[seqindex][col_i];
	   char aa2 = seqarray[seqindex][col_j];

	   if (aa1 == '-' || aa2 == '-') {continue;} // effective alignment - compare only sequences without gaps at either column

	   n_seqs++;

		 
	 } // end for seqindex

	 //	 if (n_seqs < 10) {bFlag = 1; break;}



	 if(bFlag == 0){
	   //std::cout << col_ij_mutual_info;
	   //std::cout << "\n";
	   std::cout << "ColPair ";
	   std::cout << col_i;
	   std::cout << ":";
	   std::cout << col_j;
	   std::cout << " nseqs_effaln = ";
	   std::cout << n_seqs;
	   std::cout << " sequences\n";

	 }
       } // end for col_j
     } // end for col_i
     
     // FREE ALLOCATED MEMORY AND CLOSE FILE
     for (int m = 0; m < seqcount; m++) {
       //free(seqarray[m]);
       delete [] seqarray[m];
     }
     
   return 0;
}
