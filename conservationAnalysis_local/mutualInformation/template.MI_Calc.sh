#!/bin/sh

#$ -S /bin/bash
#$ -V 
#$ -cwd
#$ -j y
#$ -r y
#$ -N blast
#$ -t 1-1
#$ -o LOGFILES
  
### here you list your tasks like  (task1 task2 ... ) separated by blanks
tasks=(30-90-30-30-60-1a3rL)

## this is default, do not touch
jobindex=$((SGE_TASK_ID-1));
input=${tasks[$jobindex]}
wd=$PWD

# parse the input
sl=`echo $input | awk -F'-' '{print $1}'`
su=`echo $input | awk -F'-' '{print $2}'`
fl=`echo $input | awk -F'-' '{print $3}'`
ll=`echo $input | awk -F'-' '{print $4}'`
pu=`echo $input | awk -F'-' '{print $5}'`
pdb_id=`echo $input | awk -F'-' '{print $6}'`
pdb_id_orig="origid"

# copy required files to remote directory
tempdir=/tmp/steven/$input"_"$JOB_ID
rm -fr $tempdir; mkdir -p $tempdir
cd $tempdir

# code goes here

# 1. Calculate MI-Score of alignment

hub_dir="/home/steven/SAMMI"

alnhubdir="$hub_dir/alignments"
alndir="$alnhubdir/$pdb_id-alns"
#alndir="$hub_dir/alignments/$pdb_id-alns"

mihubdir="$hub_dir/mutualInformation"
mioutdir="$mihubdir/$pdb_id-mi-effaln50"
#mioutdir="$mihubdir/$pdb_id-mi-effaln10"
mkdir -p $mioutdir

alndir_seqsonly="$mioutdir/$pdb_id-alns_seqsonly"
alndir_colshuffle="$mioutdir/$pdb_id-alns_seqsonly_colshuff"
mkdir -p $alndir_seqsonly
mkdir -p $alndir_colshuffle
midir_original="$mioutdir/$pdb_id-mi_original"
midir_colshuffle="$mioutdir/$pdb_id-mi_colshuff"
midir_diff="$mioutdir/$pdb_id-mi_diff"
mkdir -p $midir_original
mkdir -p $midir_colshuffle
mkdir -p $midir_diff

miscore_outfile=$mioutdir/$pdb_id.all.aln.miscore

mi_scripts_dir="$hub_dir/MI_Scripts"

# Get Seqs-Only original and column-shuffled alignments and run MI calculation on them
if [ $su -gt $sl ]; then 
    
    aln=$pdb_id.sl.$sl.su.$su.fl.$fl.ll.$ll.pu.$pu.out
    alnfile=$alndir/$aln.aln

    seqsonly_file=$alndir_seqsonly/$aln.aln.seqsonly.original.tmp
    seqsonly_file_colshuffle=$alndir_colshuffle/$aln.aln.seqsonly.colshuff.tmp

    $mi_scripts_dir/aln_fasta_seqs_only_no_gaps_query.pl $alnfile $pdb_id_orig > $seqsonly_file
    $mi_scripts_dir/aln_fasta_seqs_only_no_gaps_query_colshuffle_v2.pl $alnfile $pdb_id_orig > $seqsonly_file_colshuffle

    seqcount=`cat $seqsonly_file | wc -l`
    seqlength=`cat $seqsonly_file | head -1 | awk '{print length($0)}'`

    echo "Wrote $seqsonly_file"
    echo "Wrote $seqsonly_file_colshuffle"
		
    mi_aln_outfile=$midir_original/$aln.original.mi
    mi_aln_colshuffle_outfile=$midir_colshuffle/$aln.colshuff.mi

    g++ -O3 -funroll-loops -fstrict-aliasing -o $tempdir/aln_mi_calc_exe $mi_scripts_dir/aln_mi_calc.cpp
    $tempdir/aln_mi_calc_exe $seqsonly_file $seqcount $seqlength > $mi_aln_outfile
    $tempdir/aln_mi_calc_exe $seqsonly_file_colshuffle $seqcount $seqlength > $mi_aln_colshuffle_outfile

    echo "Wrote to $mi_aln_outfile"
    echo "Wrote to $mi_aln_colshuffle_outfile"


    mi_diff_outfile=$midir_diff/$aln.midiff
    $mi_scripts_dir/aln_mi_colpair_diff.pl $mi_aln_outfile $mi_aln_colshuffle_outfile > $mi_diff_outfile

    echo "Wrote to $mi_diff_outfile"

    ndata=`cat $mi_diff_outfile | wc -l`
    ntopXpct=`echo $ndata | awk '{printf "%d", $1*0.05}'` # top 5% midiff values
    avg_topXpct_mi=`cat $mi_diff_outfile | awk '{print $5}' | sort -g | tail -$ntopXpct | awk '{sum+=$1}END{if(NR > 0) print sum/NR}'`

    echo "$aln.aln $avg_topXpct_mi" >> $miscore_outfile
fi


## now, remove the temp dir on the node
cd 
rm -rf $tempdir
