#!/bin/sh

if [ $# -ne 1 ]; then echo "Usage: [file with sequence ids] "; exit; fi

seqidFile=$1

rm -f newjoblist
templatefile=template.MI_Calc.sh

scriptfilebase=`basename $templatefile .sh | sed 's/template/run/'`
mkdir -p LOGFILES
mkdir -p submit_script_dir
while IFS=$'\t' read -r pdb_id pdb_id_orig
  do
    for sl in 60 55 50 45 40 35 30 25 20; do
	for su in 99 90 70 50; do
	    for fl in 70; do
	        for ll in 30; do
		    for pu in 99 95 90 80 70 60 50 40; do
			if [ $su -gt $sl ]; then
			    echo "$sl-$su-$fl-$ll-$pu-$pdb_id" >> newjoblist
			fi
		    done
		done
	    done
	done
    done

    newscriptfilebase=$scriptfilebase.$pdb_id
    newscriptfile=submit_script_dir/$newscriptfilebase.sh
    ./writeSubmitscript.sh $templatefile newjoblist $pdb_id_orig > $newscriptfile;
			
    runname=mi.$pdb_id
    sed -i 's/#$ -N .*$/#$ -N '$runname'/' $newscriptfile;

    echo "written to $newscriptfilebase.sh"
    rm -f newjoblist
done <$seqidFile
