#!/bin/sh

mi_igsf_dir="/home/nelson/Desktop/Work/IgSF_Interface_Prediction/MI_IgSF"
for query_pdb_id in `cat $mi_igsf_dir/Protein_List_Used_Colon_Text_IgSF`; do

#    query_pdb_id=1I85:D
    pdb_id=`echo $query_pdb_id | sed 's/:/./'`

    pdir="$mi_igsf_dir/Output_Data-$pdb_id"
    alndir="$pdir/$pdb_id-Alignments"
    jsd_outdir="$pdir/$pdb_id-all_aln_consscores_JSD"
    mkdir -p $jsd_outdir

    pdbdir="$mi_igsf_dir/IgSF_Complex_Structures"
    pdbsadir="$mi_igsf_dir/PDB_SolAccessible/SA_gt5"

    if [ "$pdb_id" == "1I8L.A" ] || [ "$pdb_id" == "2IF7.A" ] || [ "$pdb_id" == "3BIK.A" ] || [ "$pdb_id" == "3PV6.A" ] || [ "$pdb_id" == "4FRW.B" ] ; then
	pdbfile=$pdbdir/$pdb_id.1IgFold.pdb
	pdbsafile=$pdbsadir/$pdb_id.sa.1IgFold.pdb.sagt5
    else
	pdbfile=$pdbdir/$pdb_id.pdb
	pdbsafile=$pdbsadir/$pdb_id.sa.pdb.sagt5
    fi

    for aln in `ls $alndir | sed 's/\.seqsonly\.tmp//g' | sort | uniq`; do
	jsd_aln_outfile=$jsd_outdir/$aln.JSD
	./score_conservation.py -g 0.999 -a $query_pdb_id $alndir/$aln | tail -n+5 > $jsd_aln_outfile

	more $pdbfile | awk '{print $6}' | uniq > TMP1 
	more $jsd_aln_outfile | awk '{print $3}' > TMP2 
	jsd_resnos_outfile=$jsd_outdir/$aln.resnos.JSD
	paste TMP1 TMP2 > $jsd_resnos_outfile
	rm -f TMP1 
	rm -f TMP2
	echo "Wrote $jsd_resnos_outfile"

	jsd_resnos_sa_outfile=$jsd_outdir/$aln.resnos.sagt5.JSD
	rm -f $jsd_resnos_sa_outfile
	for sa_res in `cat $pdbsafile | awk '{print $6}' | uniq`; do
	    cat $jsd_resnos_outfile | awk '{if($1=='$sa_res') print $0}' >> $jsd_resnos_sa_outfile
	done
	echo "Wrote $jsd_resnos_sa_outfile"

    done
done