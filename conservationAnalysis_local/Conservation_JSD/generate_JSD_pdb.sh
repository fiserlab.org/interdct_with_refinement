#!/bin/sh

./score_conservation.py -g 0.999 -a 1I85:B ../MI_IgSF/Output_Data-1I85.B/1I85.B-Alignments/1I85.B.sl.30.su.90.fl.30.ll.30.pu.99.out.aln | tail -n+5 > 1I85.B.FMAX.JSD

more ../IgSF_Complex_Structures/1I85.B.pdb | awk '{print $6}' | uniq > TMP1 
more 1I85.B.FMAX.JSD | awk '{print $3}' > TMP2 
paste TMP1 TMP2 > 1I85.B.FMAX.resnos.JSD 
rm -f TMP1 
rm -f TMP2

./../MI_IgSF/writeto_PDB_BfactorColumn_byresno.pl ../IgSF_Complex_Structures/1I85.B.pdb 1I85.B.FMAX.resnos.JSD > 1I85.B.FMAX.JSD.pdb

./../MI_IgSF/writeto_PDB_BfactorColumn_byresno.pl ../PDB_SolAccessible/SA_gt5/1I85.B.sa.pdb.sagt5 1I85.B.FMAX.resnos.JSD > 1I85.B.FMAX.JSD.pdb.sagt5