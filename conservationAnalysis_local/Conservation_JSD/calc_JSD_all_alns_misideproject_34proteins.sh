#!/bin/sh

mi_projdir="/home/nelson/Desktop/Work/IgSF_Interface_Prediction/MI_SideProject/parameter_search"
for pdb_id in `cat $mi_projdir/Protein_List_Used_Text | sort`; do

    query_pdb_id=`echo $pdb_id | sed 's/\./:/'`

#    pdir="$mi_projdir/Output_Data-$pdb_id"
    alndir="$mi_projdir/$pdb_id-Alignments"
    jsd_outdir="/home/nelson/Desktop/Work/IgSF_Interface_Prediction/MI_SideProject/parameter_search_no_gaps_query/$pdb_id-all_aln_consscores_JSD"
    mkdir -p $jsd_outdir

    pdbdir="$mi_projdir/$pdb_id-Structures"
    pdbsadir="$pdbdir/SCRATCH_output"

    pdbfile=$pdbdir/$pdb_id.pdb
    pdbsafile=$pdbsadir/$pdb_id.pdb.sa.acc.rsagt5

    for aln in `ls $alndir | sed 's/\.seqsonly\.tmp//g' | sort | uniq`; do
	jsd_aln_outfile=$jsd_outdir/$aln.JSD
	./score_conservation.py -g 0.999 -a $query_pdb_id $alndir/$aln | tail -n+5 > $jsd_aln_outfile

	more $pdbfile | awk '{print $6}' | uniq > TMP1 
	more $jsd_aln_outfile | awk '{print $3}' > TMP2 
	jsd_resnos_outfile=$jsd_outdir/$aln.resnos.JSD
	paste TMP1 TMP2 > $jsd_resnos_outfile
	rm -f TMP1 
	rm -f TMP2
	echo "Wrote $jsd_resnos_outfile"

	jsd_resnos_sa_outfile=$jsd_outdir/$aln.resnos.sagt5.JSD
	rm -f $jsd_resnos_sa_outfile
	for sa_res in `cat $pdbsafile | awk '{print $6}' | uniq`; do
	    cat $jsd_resnos_outfile | awk '{if($1=='$sa_res') print $0}' >> $jsd_resnos_sa_outfile
	done
	echo "Wrote $jsd_resnos_sa_outfile"

    done
done