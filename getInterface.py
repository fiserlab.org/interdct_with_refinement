import subprocess
import os 

homeDir = os.getcwd()

file = f'{homeDir}/pdbsAndChains.txt'
fh = open(file)
pdb = []
qc = []
ic = []
for f in fh:
	f = f.strip()
	pdb.append(f.split('.')[0])
	qc.append(f.split('.')[1])
	ic.append(f.split('.')[2])
fh.close()

intercaatLocation = f'{homeDir}/INTERCAAT/intercaat.py'

c = 0
while c < len(pdb):
	newFile = open(f'{homeDir}/intercaatResults/{pdb[c]}.{qc[c]}.{ic[c]}','x')
	interface = subprocess.check_output(['python3', intercaatLocation, '-pdb', f'{pdb[c]}.pdb', 
	'-qc', qc[c],'-ic', ic[c], '-fp', f'{homeDir}/PDBs/'], shell =False, text= True)
	interface = interface.split('\n')
	

	for i in interface:
		if 'Query' in i:
			break
		if 'Res' not in i:
			i = i.split()
			newFile.write(i[1]+'\n')
	newFile.close()

	c += 1
	break


