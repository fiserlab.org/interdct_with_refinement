#!/bin/bash

# Input query protein
for PDB in `cat ../pdbs.txt`; do
	p="${PDB:0:4}"
	wget https://files.rcsb.org/view/${p}.pdb
done
rm *.pdb.*
