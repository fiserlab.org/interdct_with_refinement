import glob
import os
import statistics

def getsubclusterfscores(Q,ccc):
	homeDir = os.getcwd()

	files = []
	file =  f'{homeDir}/queryClusters/random{ccc}/{Q}'
	fh = open(file)
	for f in fh:
		files.append(f.strip())
	fh.close()

	subClusterFscores = []
	for subclust in files:
		templates = []
		file2 = f'{homeDir}/queryClusters/random{ccc}/{Q}'
		fh2 = open(file2)
		for f2 in fh2:
			templates.append(f2.strip())
		fh2.close()

		skip = ''
		for t in templates:
			if subclust == t:
				skip = t
		if skip != '':
			templates.remove(skip)

		log = []
		file = f'{homeDir}/alignments/subcluster_reliability/dir_{Q}/log.{subclust}' 
		fh = open(file)
		i = 0
		indexStart = []
		indexEnd = []
		for f in fh:
			f = f.strip()
			log.append(f)
			if f'#  N av ds st dv   {subclust[0:6]}' in f:
				indexStart.append(i)
			if f'{subclust}-str.ali' in f:
				indexEnd.append(i)
			i += 1
		fh.close()

		mappedResidueCount = []
		uniqueResidues = []
		cc = 0
		while cc < len(templates):
			interface = []
			filetem = glob.glob(f'{homeDir}/intercaatResults/{templates[cc]}*')[0]
			fhtem = open(filetem)
			for ftem in fhtem:
				interface.append(ftem.strip())
			fhtem.close()
			c = indexStart[cc]
			eqRes = []
			while c < indexEnd[cc]:
				if log[c][0:1] != '#':
					hold = log[c].split()
					if '*' in hold:
						if hold[7] in interface:
							if hold[5] not in uniqueResidues:
								mappedResidueCount.append(1)
								uniqueResidues.append(hold[5])
							else:
								mappedResidueCount[uniqueResidues.index(hold[5])] += 1
					else:
						if hold[6] in interface:
							if hold[4] not in uniqueResidues:
								mappedResidueCount.append(1)
								uniqueResidues.append(hold[4])
							else:
								mappedResidueCount[uniqueResidues.index(hold[4])] += 1
				c += 1
			cc += 1
		c = 0

		factor = 0.18 #*****************************

		oldRawInterface = []
		hold = []
		filePDB = f'{homeDir}/SA_PDBs/{subclust}.SA'
		fhPDB = open(filePDB)
		for fPDB in fhPDB:
			fPDB = fPDB.strip()
			if fPDB[22:26].strip() in uniqueResidues and fPDB[22:26].strip() not in hold:
				newBFactor = mappedResidueCount[uniqueResidues.index(fPDB[22:26].strip())]
				oldRawInterface.append([fPDB[22:26].strip(),int(float(newBFactor))])
				hold.append(fPDB[22:26].strip())


		freq = int(round(len(templates)*factor,0))
		rawInterface = []
		for aa,bb in oldRawInterface:
			if bb > freq:
				rawInterface.append(int(aa))


		intercaatInterface = []
		fh2 = open(glob.glob(f'{homeDir}/intercaatResults/{subclust}*')[0])
		for f2 in fh2:
			intercaatInterface.append(int(f2))
		fh2.close()

		tp = 0
		fp = 0
		fn = 0
		for ri in rawInterface:
			if ri in intercaatInterface:
				tp += 1
			if ri not in intercaatInterface:
				fp += 1 
		for ii in intercaatInterface:
			if ii not in rawInterface:
				fn += 1

		fscore = round(tp/(tp + 0.5*(fp + fn)),3)

		subClusterFscores.append(fscore)

	return([statistics.mean(subClusterFscores),statistics.stdev(subClusterFscores)])
