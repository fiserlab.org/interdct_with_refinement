import os
import importlib.util
import sys
import subprocess

# Check python packages
for package_name in ['os','operator','itertools','glob','statistics','pandas','subprocess','numpy','scipy','random']:
	spec = importlib.util.find_spec(package_name)
	if spec is None:
		print(package_name +' is not installed')
		sys.exit()

# Check paths
modellerPath = subprocess.check_output(["cat pathsAndExecutables.txt | grep \"^modellerPath\" | cut -f2 -d= | sed 's/ //g'"],shell=True,text=True).strip()
modellerExecutable = subprocess.check_output(["cat pathsAndExecutables.txt | grep \"^modellerExecutable\" | cut -f2 -d= | sed 's/ //g'"],shell=True,text=True).strip()
clustalwPath = subprocess.check_output(["cat pathsAndExecutables.txt | grep \"^clustalwPath\" | cut -f2 -d= | sed 's/ //g'"],shell=True,text=True).strip()
clustalwExecutable = subprocess.check_output(["cat pathsAndExecutables.txt | grep \"^clustalwExecutable\" | cut -f2 -d= | sed 's/ //g'"],shell=True,text=True).strip()
naccessPath = subprocess.check_output(["cat pathsAndExecutables.txt | grep \"^naccessPath\" | cut -f2 -d= | sed 's/ //g'"],shell=True,text=True).strip()
zdockPath = subprocess.check_output(["cat pathsAndExecutables.txt | grep \"^zdockPath\" | cut -f2 -d= | sed 's/ //g'"],shell=True,text=True).strip()
cdhitPath = subprocess.check_output(["cat pathsAndExecutables.txt | grep \"^cdhitPath\" | cut -f2 -d= | sed 's/ //g'"],shell=True,text=True).strip()
psiblastPath = subprocess.check_output(["cat pathsAndExecutables.txt | grep \"^psiblastPath\" | cut -f2 -d= | sed 's/ //g'"],shell=True,text=True).strip()
nr90 = subprocess.check_output(["cat pathsAndExecutables.txt | grep \"^nr90\" | cut -f2 -d= | sed 's/ //g'"],shell=True,text=True).strip()
python2Path = subprocess.check_output(["cat pathsAndExecutables.txt | grep \"^python2Path\" | cut -f2 -d= | sed 's/ //g'"],shell=True,text=True).strip()
python2Exe = subprocess.check_output(["cat pathsAndExecutables.txt | grep \"^python2Exe\" | cut -f2 -d= | sed 's/ //g'"],shell=True,text=True).strip()

if os.path.exists(f'{modellerPath}/{modellerExecutable}') == False:
	print(f'Cannot find Modeller')
	sys.exit()
if os.path.exists(f'{clustalwPath}/{clustalwExecutable}') == False:
	print(f'Cannot find Clustalw')
	sys.exit()
if os.path.isdir(naccessPath) == False:
	print(f'Cannot find Naccess')
	sys.exit()
if os.path.isdir(zdockPath) == False:
	print(f'Cannot find ZDOCK')
	sys.exit()
if os.path.isdir(cdhitPath) == False:
	print(f'Cannot find cd-hit')
	sys.exit()
if os.path.isdir(psiblastPath) == False:
	print(f'Cannot find Psiblast')
	sys.exit()
if os.path.exists(nr90) == False:
	print(f'Cannot find nr90 database')
	sys.exit()
if os.path.exists(f'{python2Path}/{python2Exe}') == False:
	print(f'Cannot find python2')
	sys.exit()

homeDir = os.getcwd()

queries = []
file = f'{homeDir}/queries.txt'
fh = open(file)
for f in fh:
	queries.append(f.strip())
fh.close()

os.system('cat pdbs.txt queries.txt > pdbsAndQueries.txt')

# Extracts FASTA files from queries PDBs in 'structures' 
os.system('python3 extractFASTA.py')

# Runs naccess on all query structures
os.system('bash get_SA.PDB.sh')

#######################

### RUN SAMMI. Longest step. Takes ~3 hours per query
for q in queries:
	os.system(f'python3 conservationAnalysis_local/SAMMI.py {q}')

# #######################

# Renumbers residue numbers in
os.system('python3 renumber.py')

# Does all pairwasie structural alignments and sets up directories
os.system(f'bash setupSingleTempsAndDir.sh')

# Maps pairwise interface residues
os.system('python3 singleTemplateMappedResidues.py')

# Calculates pairwise fscores
os.system('python3 singleAlignFscores.py')

# Clusters queries with most similar templates
os.system('python3 predictTemplateClusters.py')

# Does all pairwise structural alignments for each query and its respective cluster of templates 
os.system(f'bash setupClusterAlignments.sh')

# Counts residues that were mapped from the clustered templates to the query 
os.system(f'python3 countMappedResidues.py')

# Makes confidence assessment and makes an interDCT prediction
os.system(f'python3 clusterConfidence.py')

# Cleans up directories
os.system(f'python3 cleanup.py')
