#!/bin/bash

zdockpath=`cat pathsAndExecutables.txt | grep "^zdockPath" | cut -f2 -d= | sed 's/ //g'`
homeDir=`pwd`
probeLocation=$homeDir/PDB_probes
receptorLocation=$homeDir/structures
probes=("1I85" "1T0P" "2JJS" "2PTT" "2WBW" "3UDW" "1WHZ" "2EAQ" "2V86" "2W8X" "2Y2Y" "3H33" "5CUK")
receptor=$1
chain=$2

cd zdockComplexes
mkdir ${receptor}.${chain}
cd $receptor.${chain}

for probe in ${probes[@]}; do
	mkdir ${receptor}_${probe}
	cd ${receptor}_${probe}
	cp ${zdockpath}/uniCHARMM ${zdockpath}/create_lig .
	${zdockpath}/mark_sur ${probeLocation}/${probe}.P ${probe}.P.l_m.pdb
	${zdockpath}/mark_sur ${receptorLocation}/${receptor}.${chain}.pdb ${receptor}.${chain}.r_m.pdb
	${zdockpath}/zdock -R ${receptor}.${chain}.r_m.pdb -L ${probe}.P.l_m.pdb -o ${receptor}_${probe}_zdock.out
	${zdockpath}/create.pl ${receptor}_${probe}_zdock.out
	cd ..
	echo  "python3 getZdockInterface.py ${receptor} ${chain} ${probe}" >> ../../zdockInterfaceJobs.txt
done

