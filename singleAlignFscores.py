import statistics
import glob
import pandas as pd
import numpy as np
import scipy.cluster.hierarchy as shc
import matplotlib.pyplot as plt
import os 

homeDir = os.getcwd()

### Gets fscores 
pdbs = []
file1 = f'{homeDir}/pdbs.txt'
fh1 = open(file1)
for f1 in fh1:
	pdbs.append(f1.strip())
fh1.close()


for pp in pdbs:
	holdf = []
	newFile = open(f'{homeDir}/singleAlignFscores/alignedTo{pp}_fscores','w')
	for p in pdbs:
		if pp != p:
			mappedRes = []
			solvAccesRes = []
			fh = open(f'{homeDir}/changedBfactorMapcount/singleTemplates/alignedTo.{pp}/{p}.SA.changedBfactorMapcount')
			for f in fh:
				if int(f[22:26]) not in solvAccesRes:
					solvAccesRes.append(int(f[22:26]))
				if float(f[60:66]) > 0:
					if int(f[22:26]) not in mappedRes:
						mappedRes.append(int(f[22:26]))
			fh.close()


			intFile = glob.glob(f'{homeDir}/intercaatResults/{p}*')[0]
			intfh = open(intFile)
			interface = []
			for x in intfh:
				x = x.strip()
				interface.append(int(x))
			intfh.close()
			tp = 0
			fp = 0
			fn = 0
			for mr in mappedRes:
				if mr in interface:
					tp += 1
				if mr not in interface:
					fp += 1 
			for i in interface:
				if i not in mappedRes:
					fn += 1
			fscore = round(tp/(tp + 0.5*(fp + fn)),3)
			holdf.append(fscore)
			newFile.write('{0:<10}{1}\n'.format(p,fscore))
	print(pp,round(statistics.mean(holdf),3))
	newFile.close()

