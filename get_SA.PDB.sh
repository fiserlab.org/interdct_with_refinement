#!/bin/bash

naccessLoc=`cat pathsAndExecutables.txt | grep "^naccessPath" | cut -f2 -d= | sed 's/ //g'`

homeDir=`pwd`
outdir=$homeDir/SA_PDBs

# Input query protein
for PDB in `cat queries.txt`; do
	queryPDB=$homeDir/structures/${PDB}.pdb

	cd ${naccessLoc}

	# (1) Removes all Hydrogen atoms from PDB
	# (2) Changes (CYX->CYS, HIE->HIS)
	awk '{if($3 !~/^H/) print $0}' $queryPDB | sed -e 's/CYX/CYS/' -e 's/HI./HIS/'  > tmp1.pdb
	#naccess $reformattedfile # output is tmp.asa
	./naccess tmp1.pdb -r ${naccessLoc} -s ${naccessLoc}

	more tmp1.asa | awk '{if($10 >= 5) print $0}' > tmp2.asa
	cp tmp2.asa $PDB.SA
	mv $PDB.SA $outdir
	rm tmp1.asa tmp1.log tmp1.pdb tmp1.rsa tmp2.asa
done
