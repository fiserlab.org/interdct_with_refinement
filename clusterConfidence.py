import os
import glob
import statistics
import subcluster_countMappedResidues


homeDir = os.getcwd()

queriesList = []
file = f'{homeDir}/queries.txt'
fh = open(file)
for f in fh:
	queriesList.append(f.strip())
fh.close()

allQueries = []
nonTrivial = []
file = f'{homeDir}/trivial.txt'
fh = open(file)
for f in fh:
	f = f.strip()
	if f.split()[1] == 'NotTrivial':
		nonTrivial.append(f.split()[0])	
	allQueries.append(f.split()[0])
fh.close()

##############################################################################

avgFscores = []
stdFscores = []
hold = []
index = -1
ccc = 1
while ccc <= 10:
	files = glob.glob(f'{homeDir}/queryClusters/random{ccc}/*')
	files = sorted(files)
	for file in files:
		if file.split('/')[-1] in queriesList:
			Q = file.split('/')[-1]
			print(ccc,Q)
			if Q not in hold:
				hold.append(Q)
			else:
				index = hold.index(Q)

			numOfTemps = 0
			fh = open(file)
			for f in fh:
				numOfTemps += 1
			fh.close()

			if numOfTemps > 1 and Q in nonTrivial:
				os.system(f'{homeDir}/runSubclusterAlignment.sh {Q} {ccc}')  #################

				hold = subcluster_countMappedResidues.getsubclusterfscores(Q,ccc)    ###############

				avgFscores.append([Q,ccc,hold[0]/10])
				stdFscores.append([Q,ccc,hold[1]/10])
			else:
				avgFscores.append([Q,ccc,1])
				stdFscores.append([Q,ccc,0])
			os.system(f'{homeDir}/cleanup_SubclusterAlignment.sh')    ##################
	ccc += 1

newFile = open(f'{homeDir}/tempFile_random.txt','w')
c = 0
while c < len(avgFscores):
	newFile.write(f'{avgFscores[c][0]} {avgFscores[c][1]} {avgFscores[c][2]} {stdFscores[c][2]}\n')
	print(avgFscores[c][0],avgFscores[c][1],avgFscores[c][2],stdFscores[c][2])
	c += 1
newFile.close()


holdName = []
holdAvg = []
holdStd = []
holdNum = []
file = f'{homeDir}/tempFile_random.txt'
fh = open(file)
for f in fh:
	f = f.strip()
	f = f.split()
	if f[0] not in holdName:
		holdName.append(f[0])
		if f[2].strip() != '1' and f[2].strip() != '0':
			holdNum.append(1)
			holdAvg.append(float(f[2]))
			holdStd.append(float(f[3]))
		else:
			holdNum.append(0)
			holdAvg.append(0)
			holdStd.append(0)
	else:
		if f[2].strip() != '1' and f[2].strip() != '0':
			holdNum[holdName.index(f[0])] += 1
			holdAvg[holdName.index(f[0])] += float(f[2])
			holdStd[holdName.index(f[0])] += float(f[3])

fh.close()

c = 0
hold = []
while c < len(holdName):
	if holdNum[c] != 0:
		hold.append([holdName[c],round(10*holdAvg[c]/holdNum[c],3),round(10*holdStd[c]/holdNum[c],3)])
	else:
		hold.append([holdName[c],1,0])
	c += 1

# for a,b,c in hold:
# 	print(a,b,c)

ccc = 0
conf = []
stdFactor = 0.175
avgFactor = 0.525
newFile = open(f'{homeDir}/confidence.txt','w')
while ccc < len(hold):
	if hold[ccc][1] != 1 and hold[ccc][0] != 0:
		if hold[ccc][1] > avgFactor and hold[ccc][2] < stdFactor:
			conf.append(3)
			newFile.write(f'{hold[ccc][0]} HighConfidence avg:{round(hold[ccc][1],3)} std:{round(hold[ccc][2],3)}\n')
		elif (hold[ccc][1] > avgFactor and hold[ccc][2] >= stdFactor) or (hold[ccc][1] <= avgFactor and hold[ccc][2] < stdFactor):
			conf.append(2)
			newFile.write(f'{hold[ccc][0]} MediumConfidence avg:{round(hold[ccc][1],3)} std:{round(hold[ccc][2],3)}\n')
		elif hold[ccc][1] <= avgFactor and hold[ccc][2] >= stdFactor:
			conf.append(1)
			newFile.write(f'{hold[ccc][0]} LowConfidence avg:{round(hold[ccc][1],3)} std:{round(hold[ccc][2],3)}\n')
	else:
		conf.append(3)
		newFile.write(f'{hold[ccc][0]} HighConfidence Trivial\n')
	ccc += 1
newFile.close()

##############################################################################


# Get Interface Prediction
interfaceFile = open(f'{homeDir}/interfacePredictions.txt','w')
pdbs = []
file = f'{homeDir}/queries.txt'
fh = open(file)
for f in fh:
	pdbs.append(f.strip())
fh.close()

factor = 0.18 #*****************************
for Q in pdbs:
	oldRawInterface = []
	hold = []
	fh = open(f'{homeDir}/changedBfactorMapcount/queries/{Q}.SA.changedBfactorMapcount')
	for f in fh:
		if float(f[60:66]) > 0 and f[22:26].strip() not in hold:
			oldRawInterface.append([f[22:26].strip(),int(float(f[61:66]))])
			hold.append(f[22:26].strip())
	fh.close()

	subclustSize = 0
	fh = open(f'{homeDir}/queryClusters/queries/{Q}')
	for f in fh:
		subclustSize += 1
	fh.close() 


	freq = int(round(subclustSize*factor,0))
	rawInterface = ''
	for aa,bb in oldRawInterface:
		if bb > freq:
			rawInterface += f',{aa}'

	print('{0:<12}{1}'.format(Q,rawInterface[1:]))
	interfaceFile.write('{0:<12}{1}\n'.format(Q,rawInterface[1:]))


