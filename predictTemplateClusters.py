import glob
import numpy as np
import matplotlib.pyplot as plt
import scipy.cluster.hierarchy as shc
import sys
import subprocess
import statistics
import operator
import os
import random

homeDir = os.getcwd()

queries = []
file = f'{homeDir}/queries.txt'
fh = open(file)
for f in fh:
	queries.append(f.strip())
fh.close()

trivialFile = open(f'{homeDir}/trivial.txt','w')
N = 8
for Q in queries:
	pdbs = []
	file1 = f'{homeDir}/pdbs.txt'
	fh1 = open(file1)
	for f1 in fh1:
		if f1.strip() != Q:
			pdbs.append(f1.strip())
	fh1.close()
	pdbs.append(Q)


	sammi2d = []
	resCountMatrix = []
	for pp in pdbs:
		sammi = []
		pdba = pp.split('.')[0]
		pdbb = pp.split('.')[1]            
		sammi_file = f'{homeDir}/conservationAnalysis_local/renumbered_scores/{pdba}:{pdbb}.conservation.renumbered'
		sfh = open(sammi_file)
		for sf in sfh:
			sf = sf.strip()
			sammi.append([str(int(sf.split()[0])),float(sf.split()[2])])
		sfh.close()
		sammi2d.append(sammi)

		if pp != Q:
			rc_file = open(f'{homeDir}/predIntAlign/interfaceResMappedCounts/{pp}.ResCounts')
			hold = []
			for rf in rc_file:
				rf = rf.strip()
				hold.append([int(rf.split()[0]),int(rf.split()[1])])
			rc_file.close()
			hold = sorted(hold, key = operator.itemgetter(0), reverse=True)
			hold2 = []
			for hh1,hh2 in hold:
				hold2.append(hh2)
			resCountMatrix.append(hold2[0:N])
		else:
			resCountMatrix.append([-1,-1,-1,-1,-1,-1,-1,-1])	
	resCountMatrix = np.array(resCountMatrix)

	c = 0
	averageScoresMatrix = np.zeros((len(pdbs),len(pdbs)))
	while c < N:
		scoresMatrix = np.zeros((len(pdbs),len(pdbs)))
		columnCount = 0
		while columnCount < len(pdbs):
			rowCount = 0
			while rowCount < len(pdbs)-1:
				res = ''
				if rowCount == columnCount:
					fh = open(glob.glob(f'{homeDir}/predIntAlign/alignedTo.{pdbs[rowCount]}/*')[0])
					for ff in fh:
						ff = ff.strip()
						if int(float(ff.split()[2])) == resCountMatrix[rowCount,c]:
							res = str(int(float(ff.split()[0])))
					fh.close()
				else:
					fh = open(f'{homeDir}/predIntAlign/alignedTo.{pdbs[rowCount]}/{pdbs[columnCount]}.ali')
					for ff in fh:
						ff = ff.strip()
						if int(float(ff.split()[2])) == resCountMatrix[rowCount,c]:
							res = str(int(float(ff.split()[1])))
					fh.close()

				for sammiRes,sammiScore in sammi2d[columnCount]:
					if sammiRes == res:
						if sammiScore != -1000:
							scoresMatrix[columnCount,rowCount] = sammiScore
						else:
							scoresMatrix[columnCount,rowCount] = 0
						break
				rowCount += 1
			columnCount += 1
		averageScoresMatrix += scoresMatrix
		c += 1
	averageScoresMatrix = averageScoresMatrix.tolist()



	indexOrderSAMMI = []
	for ss in averageScoresMatrix:
		hold = np.zeros((len(ss)))
		hold.fill(len(pdbs))
		c = 1
		while sum(ss) > 0:
			i = ss.index(max(ss))
			hold[i] = c
			ss[i] = 0
			c += 1
		hold = [int(j) for j in hold]
		indexOrderSAMMI.append(hold)


	#########################



	# ******** NEWTREE ***********#
	# plt.figure(figsize=(10, 7))  
	# plt.title(f"{Q}")
	# dend = shc.dendrogram(shc.linkage(indexOrderSAMMI, method='weighted',metric = 'correlation'), labels = pdbs, leaf_rotation=90., leaf_font_size=10.)
	# # plt.gca().set_ylim([0, 1.4])
	# # plt.savefig(f'/home/steven/Desktop/Screenshots/SAMMI_no_query_Dend.png', bbox_inches='tight')
	# plt.show()
	# # break

	heights = range(0, 1000, 1)
	heights = [y/1000+0.0001 for y in heights]
	members = []
	memberLen = []
	memberHeights = []
	for hei in heights:
		cuttree = shc.cut_tree(shc.linkage(indexOrderSAMMI, method='weighted',metric = 'correlation'), height = hei)

		queryTree = cuttree[-1][0]
		c = 0
		hold = []
		while c < len(cuttree)-1:
			if cuttree[c][0] == queryTree:
				hold.append(pdbs[c])
			c += 1

		if hold not in members and hold != []:
			members.append(hold)
			memberLen.append(len(hold))
			memberHeights.append(round(hei,4))

	if len(memberHeights) > 1:
		mod = 0
		c = 2
		holdDiff = [0,memberHeights[1]-memberHeights[0]]
		while c < len(members):
			if memberHeights[c] - memberHeights[c-1] - mod > holdDiff[1]:
				holdDiff = [c-1,memberHeights[c] - memberHeights[c-1] - mod]
			c += 1
			mod += 0.0
	else:
		holdDiff = [0,0]
	if members == []:
		members.append(pdbs[0:-1])
		memberHeights.append(1)
		memberLen.append(len(pdbs[0:-1]))
	seqIdenList = []
	for mm in members: 
		hold = []
		for m in mm:
			seqIden = subprocess.check_output(['bash',f'{homeDir}/sequenceIdentity.sh',Q,m],shell =False, text= True)
			seqIden = float(seqIden.split('\n')[-2])
			hold.append(seqIden)
		seqIdenList.append(hold)

	newFile = open(f'{homeDir}/queryClusters/queries/{Q}','w')
	if max(seqIdenList[-1]) > 0.35:
		c = 0
		for si in seqIdenList[-1]:
			if si > 0.35 and si > max(seqIdenList[-1])-0.2:
				newFile.write(f'{members[-1][c]}\n')
			c += 1
		trivialFile.write(f'{Q}  Trivial\n')
		print(Q,'Trivial')
	else:
		trivialFile.write(f'{Q}  NotTrivial\n')
		print(Q,'NotTrivial')
		for m in members[holdDiff[0]]:
			newFile.write(f'{m}\n')
	newFile.close
trivialFile.close()






###################### To get confidence measure

ccc = 1
while ccc <= 10:
	N = 8
	for Q in queries:
		pdbs = []
		file1 = f'{homeDir}/pdbs.txt'
		fh1 = open(file1)
		for f1 in fh1:
			if f1.strip() != Q:
				pdbs.append(f1.strip())
		fh1.close()
		pdbs.append(Q)


		sammi2d = []
		resCountMatrix = []
		for pp in pdbs:
			sammi = []
			pdba = pp.split('.')[0]
			pdbb = pp.split('.')[1]            
			sammi_file = f'{homeDir}/conservationAnalysis_local/renumbered_scores/{pdba}:{pdbb}.conservation.renumbered'
			sfh = open(sammi_file)
			for sf in sfh:
				sf = sf.strip()
				sammi.append([str(int(sf.split()[0])),float(sf.split()[2])])
			sfh.close()
			sammi2d.append(sammi)

			if pp != Q:
				rc_file = open(f'{homeDir}/predIntAlign/interfaceResMappedCounts/{pp}.ResCounts')
				hold = []
				for rf in rc_file:
					rf = rf.strip()
					hold.append([int(rf.split()[0]),int(rf.split()[1])])
				rc_file.close()
				hold = sorted(hold, key = operator.itemgetter(0), reverse=True)
				hold2 = []
				for hh1,hh2 in hold:
					hold2.append(hh2)
				resCountMatrix.append(random.sample(hold2, 8))
			else:
				resCountMatrix.append([-1,-1,-1,-1,-1,-1,-1,-1])	
		resCountMatrix = np.array(resCountMatrix)

		c = 0
		averageScoresMatrix = np.zeros((len(pdbs),len(pdbs)))
		while c < N:
			scoresMatrix = np.zeros((len(pdbs),len(pdbs)))
			columnCount = 0
			while columnCount < len(pdbs):
				rowCount = 0
				while rowCount < len(pdbs)-1:
					res = ''
					if rowCount == columnCount:
						fh = open(glob.glob(f'{homeDir}/predIntAlign/alignedTo.{pdbs[rowCount]}/*')[0])
						for ff in fh:
							ff = ff.strip()
							if int(float(ff.split()[2])) == resCountMatrix[rowCount,c]:
								res = str(int(float(ff.split()[0])))
						fh.close()
					else:
						fh = open(f'{homeDir}/predIntAlign/alignedTo.{pdbs[rowCount]}/{pdbs[columnCount]}.ali')
						for ff in fh:
							ff = ff.strip()
							if int(float(ff.split()[2])) == resCountMatrix[rowCount,c]:
								res = str(int(float(ff.split()[1])))
						fh.close()

					for sammiRes,sammiScore in sammi2d[columnCount]:
						if sammiRes == res:
							if sammiScore != -1000:
								scoresMatrix[columnCount,rowCount] = sammiScore
							else:
								scoresMatrix[columnCount,rowCount] = 0
							break
					rowCount += 1
				columnCount += 1
			averageScoresMatrix += scoresMatrix
			c += 1
		averageScoresMatrix = averageScoresMatrix.tolist()



		indexOrderSAMMI = []
		for ss in averageScoresMatrix:
			hold = np.zeros((len(ss)))
			hold.fill(len(pdbs))
			c = 1
			while sum(ss) > 0:
				i = ss.index(max(ss))
				hold[i] = c
				ss[i] = 0
				c += 1
			hold = [int(j) for j in hold]
			indexOrderSAMMI.append(hold)


		#########################



		# ******** NEWTREE ***********#
		# plt.figure(figsize=(10, 7))  
		# plt.title(f"{Q}")
		# dend = shc.dendrogram(shc.linkage(indexOrderSAMMI, method='weighted',metric = 'correlation'), labels = pdbs, leaf_rotation=90., leaf_font_size=10.)
		# # plt.gca().set_ylim([0, 1.4])
		# # plt.savefig(f'/home/steven/Desktop/Screenshots/SAMMI_no_query_Dend.png', bbox_inches='tight')
		# plt.show()
		# # break

		heights = range(0, 1000, 1)
		heights = [y/1000+0.0001 for y in heights]
		members = []
		memberLen = []
		memberHeights = []
		for hei in heights:
			cuttree = shc.cut_tree(shc.linkage(indexOrderSAMMI, method='weighted',metric = 'correlation'), height = hei)

			queryTree = cuttree[-1][0]
			c = 0
			hold = []
			while c < len(cuttree)-1:
				if cuttree[c][0] == queryTree:
					hold.append(pdbs[c])
				c += 1

			if hold not in members and hold != []:
				members.append(hold)
				memberLen.append(len(hold))
				memberHeights.append(round(hei,4))

		if len(memberHeights) > 1:
			mod = 0
			c = 2
			holdDiff = [0,memberHeights[1]-memberHeights[0]]
			while c < len(members):
				if memberHeights[c] - memberHeights[c-1] - mod > holdDiff[1]:
					holdDiff = [c-1,memberHeights[c] - memberHeights[c-1] - mod]
				c += 1
				mod += 0.0
		else:
			holdDiff = [0,0]
		if members == []:
			members.append(pdbs[0:-1])
			memberHeights.append(1)
			memberLen.append(len(pdbs[0:-1]))
		seqIdenList = []
		for mm in members: 
			hold = []
			for m in mm:
				seqIden = subprocess.check_output(['bash',f'{homeDir}/sequenceIdentity.sh',Q,m],shell =False, text= True)
				seqIden = float(seqIden.split('\n')[-2])
				hold.append(seqIden)
			seqIdenList.append(hold)

		newFile = open(f'{homeDir}/queryClusters/random{ccc}/{Q}','w')
		if max(seqIdenList[-1]) > 0.35:
			c = 0
			for si in seqIdenList[-1]:
				if si > 0.35 and si > max(seqIdenList[-1])-0.2:
					newFile.write(f'{members[-1][c]}\n')
				c += 1
		else:
			for m in members[holdDiff[0]]:
				newFile.write(f'{m}\n')
		newFile.close
	ccc += 1

