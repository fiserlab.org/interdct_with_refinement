import subprocess
import os
import sys

homeDir = os.getcwd()

intercaatLocation = f'{homeDir}/INTERCAAT/intercaat.py'

receptor = sys.argv[1]
qc = sys.argv[2]
pro = sys.argv[3]
ic = 'P'

newFile = open(f"{homeDir}/zdockComplexes/{receptor}.{qc}/{receptor}_{pro}/model_allres.table", "w")
c = 1
while c <= 2000:
	try:
		interface = subprocess.check_output(['python3', intercaatLocation, '-pdb', f'complex.{str(c)}.pdb', '-qc', qc,\
		'-ic', ic, '-fp', f'{homeDir}/zdockComplexes/{receptor}.{qc}/{receptor}_{pro}/'], shell =False, text= True)
		interface = interface.split('\n')
		holdRes = f'{str(c)}:'
		for i in interface:
			if 'Query' in i:
				break
			if 'Res' not in i:
				i = i.split()
				holdRes += f'{i[1]}_{i[0]}-'
		holdRes = holdRes[0:-1]
		newFile.write(holdRes+'\n')
		c += 1
	except:
		holdRes = f'{str(c)}:*******FAIL*******'
		newFile.write(holdRes+'\n')
		c += 1

c = 0
newFile.close()
