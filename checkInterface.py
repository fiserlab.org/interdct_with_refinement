import sys
import os
import operator
from itertools import combinations

### Test Cases
homeDir = os.getcwd()

probes = ['1I85','1T0P','2JJS','2PTT','2WBW','3UDW','1WHZ','2EAQ','2V86','2W8X','2Y2Y','3H33','5CUK']

pdbsAndInterfaces = []
file = f'{homeDir}/interfacePredictions.txt'
fh = open(file)
c = 0
for f in fh:
	f = f.strip()
	pdbsAndInterfaces.append(f.split())
	c += 1
fh.close()

TT = 0.5
FF = 0.35
TT = round(TT,2)
FF = round(FF,2)
newFile = open(f'{homeDir}/almostFinalInterfaces.txt','w')
for xx,yy in pdbsAndInterfaces:
	predictedInterface = yy.split(',')
	residueFreq = [0 for y in predictedInterface]
	results = []
	q = xx.split('.')[0]
	c = 0
	while c < len(probes):	
		file = f'{homeDir}/zdockComplexes/{xx}/{q}_{probes[c]}/model_allres.table'
		fh = open(file)
		for f in fh:
			f = f.strip()
			if len(f.split(':')) > 1:
				complex_interface = f.split(':')[1]
				hold = ''
				for ci in complex_interface:
					if ci.isdigit():
						hold += ci
					if ci == '_':
						hold += ci
				hold = hold.split('_')
				complex_interface = hold[0:-1]

				tp = 0
				fp = 0
				fn = 0
				for ri in complex_interface:
					if ri in predictedInterface:
						tp += 1
					if ri not in predictedInterface:
						fp += 1 
				for ii in predictedInterface:
					if ii not in complex_interface:
						fn += 1

				fscore = round(tp/(tp + 0.5*(fp + fn)),3)
			else:
				tp = 0
				fp = 0
				fn = 0
				complex_interface = ''
				fscore = 0

			results.append([q,probes[c],f.split(':')[0],tp,fp,fn,fscore,complex_interface])
		fh.close()
		c += 1

	results = sorted(results, key = operator.itemgetter(6,3), reverse=True)

	TP_factor = TT
	FP_factor = FF
	
	falsePositives = []
	falsePositivesFreq = []
	numberOfComplexes = 1
	while numberOfComplexes <= 100: 
		for hh in results[numberOfComplexes][7]:
			if hh in predictedInterface:
				residueFreq[predictedInterface.index(hh)] += 1
			elif hh in falsePositives:
				falsePositivesFreq[falsePositives.index(hh)] += 1
			else:
				falsePositives.append(hh)
				falsePositivesFreq.append(1)
		numberOfComplexes += 1
	print(f'Total Predicted Interface Residues for {q} = {len(predictedInterface)}')
	newInterface = ''
	c = 0
	while c < len(predictedInterface):
		if residueFreq[c] >= TP_factor*numberOfComplexes:
			newInterface += f'{predictedInterface[c]},'
		c += 1

	c = 0
	while c < len(falsePositives):
		if falsePositivesFreq[c] >= FP_factor*numberOfComplexes:
			newInterface += f'{falsePositives[c]},'
		c += 1

	ll = len(newInterface[0:-1].split(','))
	newFile.write(f'{xx}  {ll}  {newInterface[0:-1]}\n')
newFile.close()
