import glob
import os 

homeDir = os.getcwd()

pdbs = []
file = f'{homeDir}/queries.txt'
fh = open(file)
for f in fh:
	pdbs.append(f.strip())
fh.close()


start = []
for p in pdbs:
	file = f'{homeDir}/structures/{p}.pdb'
	fh = open(file)
	for f in fh:
		if f[0:4] == 'ATOM':
			start.append(int(f[22:26]))
			fh.close()
			break

c = 0
for p in pdbs:
	pdba = p.split('.')[0]
	pdbb = p.split('.')[1]
	file = f'{homeDir}/conservationAnalysis_local/scores/{pdba}:{pdbb}.conservation'
	fh = open(file)
	newfile = open(f'{homeDir}/conservationAnalysis_local/renumbered_scores/{pdba}:{pdbb}.conservation.renumbered','w')
	cc = 0
	for f in fh:
		if cc > 3:
			f = f.split()
			newfile.write(f'{start[c]+cc-4}\t{f[1]}\t{f[2]}\n')
		cc += 1
	fh.close()
	newfile.close()
	c += 1

