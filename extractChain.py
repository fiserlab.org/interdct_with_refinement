import pandas as pd
import os

homeDir = os.getcwd()

df = pd.read_excel(f"{homeDir}/toExtract.ods", engine = "odf")

# general information
pdb = df["PDB"].values.tolist()
qc = df["Query Chain(s)"].values.tolist()
fold = df["Ig Fold"].values.tolist()

c = 0
while c < len(pdb):
    file = f'{homeDir}/PDBs/' + pdb[c] + '.pdb'
    newFile = open(f"{homeDir}/structures/{pdb[c]}.{qc[c]}.pdb", "x")
    fh = open(file)
    start = 0
    end = 0
    if fold[c] != 'n':
        start = int(fold[c].split('-')[0])
        end = int(fold[c].split('-')[1])
    for f in fh:
    	f = f.strip()
    	if f[0:4] != 'ATOM' and f[0:3] != 'TER' and f[0:6] != 'HETATM' and f[0:6] != 'ANISOU':
                newFile.write(f+'\n')
    	else:
                if start == 0 and end == 0:
                    if f[21:22] == qc[c] and f[0:6] != 'HETATM' and f[0:6] != 'ANISOU':
                        newFile.write(f+'\n')
                        #print(f[0:21]+'P'+f[22:78])
                else:
                    if f[21:22] == qc[c] and int(f[22:26]) >= start and int(f[22:26]) <= end and f[0:6] != 'HETATM' and f[0:6] != 'ANISOU':
                        newFile.write(f+'\n')
                        #print(f[0:21]+'P'+f[22:78])
    fh.close()
    newFile.close()
    c += 1
