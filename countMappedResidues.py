import glob
import sys
import os

homeDir = os.getcwd()

files = []
file =  f'{homeDir}/queries.txt'
fh = open(file)
for f in fh:
	files.append(f.strip())
fh.close()


current = 'queries'

for Q in files:
	templates = []
	file2 = f'{homeDir}/queryClusters/{current}/{Q}'
	fh2 = open(file2)
	for f2 in fh2:
		templates.append(f2.strip())
	fh2.close()

	skip = ''
	for t in templates:
		if Q == t:
			skip = t
	if skip != '':
		templates.remove(skip)

	log = []
	file = f'{homeDir}/alignments/{current}/log.{Q}' 
	fh = open(file)
	i = 0
	indexStart = []
	indexEnd = []
	for f in fh:
		f = f.strip()
		log.append(f)
        # May need to change {Q[0:6]} #################
		if f'#  N av ds st dv   {Q[0:6]}' in f:
			indexStart.append(i)
		if f'{Q}-str.ali' in f:
			indexEnd.append(i)
		i += 1
	fh.close()

	mappedResidueCount = []
	uniqueResidues = []
	cc = 0
	while cc < len(templates):
		interface = []
		filetem = glob.glob(f'{homeDir}/intercaatResults/{templates[cc]}*')[0]
		fhtem = open(filetem)
		for ftem in fhtem:
			interface.append(ftem.strip())
		fhtem.close()
		c = indexStart[cc]
		eqRes = []
		while c < indexEnd[cc]:
			if log[c][0:1] != '#':
				hold = log[c].split()
				if '*' in hold:
					if hold[7] in interface:
						if hold[5] not in uniqueResidues:
							mappedResidueCount.append(1)
							uniqueResidues.append(hold[5])
						else:
							mappedResidueCount[uniqueResidues.index(hold[5])] += 1
				else:
					if hold[6] in interface:
						if hold[4] not in uniqueResidues:
							mappedResidueCount.append(1)
							uniqueResidues.append(hold[4])
						else:
							mappedResidueCount[uniqueResidues.index(hold[4])] += 1
			c += 1
		cc += 1

	newFile = open(f"{homeDir}/changedBfactorMapcount/{current}/{Q}.SA.changedBfactorMapcount", "w")
	filePDB = f'{homeDir}/SA_PDBs/{Q}.SA'
	fhPDB = open(filePDB)
	for fPDB in fhPDB:
		fPDB = fPDB.strip()
		if fPDB[22:26].strip() in uniqueResidues:
			newBFactor = str(mappedResidueCount[uniqueResidues.index(fPDB[22:26].strip())])
			newFile.write('{0}{1:>6}{2:>6}\n'.format(fPDB[0:54],'1.00',newBFactor+'.00'))
		else:
			newFile.write('{0}{1:>6}{2:>6}\n'.format(fPDB[0:54],'1.00','0.00'))
	newFile.close()



ccc = 1
while ccc <= 10:
	for Q in files:
		templates = []
		file2 = f'{homeDir}/queryClusters/random{ccc}/{Q}'
		fh2 = open(file2)
		for f2 in fh2:
			templates.append(f2.strip())
		fh2.close()

		skip = ''
		for t in templates:
			if Q == t:
				skip = t
		if skip != '':
			templates.remove(skip)

		log = []
		file = f'{homeDir}/alignments/random{ccc}/log.{Q}' 
		fh = open(file)
		i = 0
		indexStart = []
		indexEnd = []
		for f in fh:
			f = f.strip()
			log.append(f)
	        # May need to change {Q[0:6]} #################
			if f'#  N av ds st dv   {Q[0:6]}' in f:
				indexStart.append(i)
			if f'{Q}-str.ali' in f:
				indexEnd.append(i)
			i += 1
		fh.close()

		mappedResidueCount = []
		uniqueResidues = []
		cc = 0
		while cc < len(templates):
			interface = []
			filetem = glob.glob(f'{homeDir}/intercaatResults/{templates[cc]}*')[0]
			fhtem = open(filetem)
			for ftem in fhtem:
				interface.append(ftem.strip())
			fhtem.close()
			c = indexStart[cc]
			eqRes = []
			while c < indexEnd[cc]:
				if log[c][0:1] != '#':
					hold = log[c].split()
					if '*' in hold:
						if hold[7] in interface:
							if hold[5] not in uniqueResidues:
								mappedResidueCount.append(1)
								uniqueResidues.append(hold[5])
							else:
								mappedResidueCount[uniqueResidues.index(hold[5])] += 1
					else:
						if hold[6] in interface:
							if hold[4] not in uniqueResidues:
								mappedResidueCount.append(1)
								uniqueResidues.append(hold[4])
							else:
								mappedResidueCount[uniqueResidues.index(hold[4])] += 1
				c += 1
			cc += 1

		newFile = open(f"{homeDir}/changedBfactorMapcount/random{ccc}/{Q}.SA.changedBfactorMapcount", "w")
		filePDB = f'{homeDir}/SA_PDBs/{Q}.SA'
		fhPDB = open(filePDB)
		for fPDB in fhPDB:
			fPDB = fPDB.strip()
			if fPDB[22:26].strip() in uniqueResidues:
				newBFactor = str(mappedResidueCount[uniqueResidues.index(fPDB[22:26].strip())])
				newFile.write('{0}{1:>6}{2:>6}\n'.format(fPDB[0:54],'1.00',newBFactor+'.00'))
			else:
				newFile.write('{0}{1:>6}{2:>6}\n'.format(fPDB[0:54],'1.00','0.00'))
		newFile.close()
	ccc += 1
