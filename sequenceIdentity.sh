#!/bin/bash

clustalwPath=`cat pathsAndExecutables.txt | grep "^clustalwPath" | cut -f2 -d= | sed 's/ //g'`
clustalwExe=`cat pathsAndExecutables.txt | grep "^clustalwExecutable" | cut -f2 -d= | sed 's/ //g'`

cat ./FASTAbyChain/$1.fasta | awk '{ print } ' > temp
cat ./FASTAbyChain/$2.fasta >> temp
${clustalwPath}/${clustalwExe} temp
perl checkPctid temp.aln
rm temp*
