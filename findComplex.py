import os
import operator
import re

homeDir = os.getcwd()

QandI = []
file = f'{homeDir}/almostFinalInterfaces.txt'
fh = open(file)
for f in fh:
	f = f.strip()
	QandI.append(f.split())
fh.close()

probes = ['1I85','1T0P','2JJS','2PTT','2WBW','3UDW','1WHZ','2EAQ','2V86','2W8X','2Y2Y','3H33','5CUK']
complexHold = []
for xx,yy,zz in QandI:
	predictedInterface = zz.split(',')
	q = xx.split('.')[0]
	c = 0

	while c < len(probes):	
		file = f'{homeDir}/zdockComplexes/{xx}/{q}_{probes[c]}/model_allres.table'
		fh = open(file)
		for f in fh:
			f = f.strip()
			complexNum = f.split(':')[0]
			if len(f.split(':')) > 1:
				complex_interface = f.split(':')[1]
				hold = ''
				for ci in complex_interface:
					if ci.isdigit():
						hold += ci
					if ci == '_':
						hold += ci
				hold = hold.split('_')
				complex_interface = hold[0:-1]

				tp = 0
				fp = 0
				fn = 0
				for ri in complex_interface:
					if ri in predictedInterface:
						tp += 1
					if ri not in predictedInterface:
						fp += 1 
				for ii in predictedInterface:
					if ii not in complex_interface:
						fn += 1

				fscore = tp/(tp + 0.5*(fp + fn))

				if fscore > 0.8:
					complexHold.append([f'zdockComplexes/{xx}/{q}_{probes[c]}/complex.{complexNum}.pdb',int(complexNum), fscore,tp,fp,fn])
		fh.close()
		c += 1

complexHold = sorted(complexHold, key = operator.itemgetter(1), reverse=False)
complexHold = sorted(complexHold, key = operator.itemgetter(2), reverse=True)

newFile = open(f'{homeDir}/FINAL.txt','w')
print('{0:<80}{1:<14}{2:<14}{3:<14}{4:<14}'.format('Complex Location','fscore','True Pos','False Pos','False Neg'))
holdqq = []
for aa,bb,cc,dd,ee,ff in complexHold:
	qq = aa.split('/')[-3]
	if holdqq.count(qq) < 1:
		print('{0:<80}{1:<14}{2:<14}{3:<14}{4:<14}'.format(aa,round(cc,3),dd,ee,ff))
		holdqq.append(qq)

		holdInt = ''
		loc_old = aa.split('/')[0:-1]
		loc = ''
		for ll in loc_old:
			loc += ll+'/'
		fh = open(loc+'model_allres.table')
		for f in fh:
			if f.split(':')[0] == str(bb):
				hh = f.split(':')[1]
				for hhh in hh.split('_'):
					if len(re.findall(r'\d+', hhh)) == 1:
						holdInt += re.findall(r'\d+', hhh)[0]+','
				break
		fh.close()
		holdInt = holdInt[0:-1]
		newFile.write(f'{qq}  {homeDir}/{aa}  {holdInt}\n')
newFile.close()






